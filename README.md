# SRO SUITE
- Our official discord group: https://discord.gg/GetEGxtYjT

## Prerequesites
- NodeJS (>16 LTS): https://nodejs.org/
- Silkroad Online game client & server (2015 ISRO-R version)
- VS Code https://code.visualstudio.com/

# Quick step by step guide to launch sro-suite modules
1. Download and install NodeJS (preferably latest LTS version)
2. Checkout the repository from https://gitlab.com/sroparadise/sro-suite
3. Open the repository folder with VSCode
4. Extract silkroad-security.zip in the root directory of the project (if you want to skip the `install boost` & build security part) 
5. Rename `.env.example` to `.env` and insert your database config accordingly to the sample provided.
6. Create a database named `SILKROAD_PROXY` (or as for your liking, but write than in .env)
7. Open the terminal in VS Code and run this command `npm i -g yarn` when finished run `yarn` and wait for it to complete (this installs the package manager used in project and the project dependencies)
8. Launch DataServer in development mode - in console: `yarn dev:DataServer` and make sure it boots up without errors.
9. When DataServer is working you can run all other modules in any order (note that almost all modules require DataServer to be on for them to actually work).

## Launch in development mode
This enables hot-reloading the code on changes (note this will disconnect you from game when you save a file).

- GatewayServer `yarn dev:GatewayServer`
- AgentServer `yarn dev:AgentServer`
- WebApps `yarn dev:WebApps`
- EventManager `yarn dev:Events`
- DiscordBot `yarn dev:Discord`

## Launch in production mode
- DataServer `yarn DataServer`
- GatewayServer `yarn GatewayServer`
- AgentServer `yarn AgentServer:1`, `yarn AgentServer:2`, `yarn AgentServer:3`, `yarn AgentServer:4`, 
- WebApps `yarn WebApps`
- EventManager `yarn Events`
- DiscordBot `yarn Discord`

_Note:_ you can see package.json scripts section for understanding more about launching modules or `launchers` directory for doing this from .bat and similar.

# Setup & run admin app
- Open another terminal and do `cd admin-app`
- Type `yarn` and wait for it to install the dependencies
- Type `yarn:dev` and the webserver will start showing you the dev url for your app
- Have fun.

## Build
`yarn build` - this builds the packaged/production version of the `src` package, when you deploy your production version only this part should be served and hosted on server, you can also apply encryption or other similar top level override on that.

## DataServer:
- DataServer provides you with swagger documentation to understand what requests/responses you need to build/parse to communicate with the database efficiently http://localhost:6440/documentation/index.html
- DataServer's purpose is to communicate directly with database whlist providing easy way to have a CRUD API together with that (you need to add and import a model)
- Please refer to `src/lib/DataServer` and `src/lib/DataServer/models` for that.
- You can run all modules from `.bat` files or command line, best if you use `git bash` or similar.
- Modules are not meant to have an UI, usually for performance purposes you may want to run this in a process manager with cluster mode enabled.
- You can use administration interface over web for that see my other project: https://gitlab.com/sroparadise/sro-suite-admin - this supports sending packets from web directly into connected clients or their server connection, currently streamlining packets from clients is not supported (might happen in future).

## Available environment variables:
`IMPORTANT NOTE`: Following variables are not supposed to be stored in `.env` file - that will apply the rule to completely all modules and it will cause you headache.

---


`MODULE` Applies to all modules in `src/services` folder, this technically tells the launcher to use `SERVICE_NAME` as the module you want to run.
Module can be one of `GatewayServer`, `AgentServer`, `WebApps` or `DataServer` - you can create your own by adding a file according to the setup of the previously mentioned.

_Example:_
```
MODULE=GatewayServer
```

- `BIND_PORT` Indicates which port you would like the module to listen at, applies to services `GatewayServer`, `AgentServer`, `WebApps` or `DataServer`.

_Example:_
```
PORT=4000
```

- `BIND_IP` Indicates which IP address you would like the module to listen at, applies to services: `GatewayServer`, `AgentServer`, `WebApps` or `DataServer`.

_Example:_
```
BIND_IP="0.0.0.0"
```

- `REMOTE_PORT` Indicates which remote port proxy will connect users to, applies to services `GatewayServer`, `AgentServer`.

_Example:_
```
REMOTE_PORT=4000
```

- `REMOTE_IP` Indicates which remote IP address proxy will connect users to, applies to services: `GatewayServer`, `AgentServer`.

_Example:_
```
REMOTE_IP="0.0.0.0"
```

`WORKER_COUNT` Applies to ll modules in `src/services` folder, this tells the engine on how many CPUs to use for running/processing each module (individually).

_Example:_
```
WORKER_COUNT=3
```

## Database Info:
- By default you will need a database named `SILKROAD_PROXY` for all proxy features to work - if you don't like it - it is easy to adjust that by setting database variable `DB_NAME=YOUR_DB_NAME`.
- To connect to SQL Server database over IP you can use this:
```
DB_HOST="127.0.0.1"
DB_USERNAME="sa"
DB_PASSWORD="YOUR_PASSWORD"
DB_DIALECT="mssql"
```

- To connect to SQL Server database over NAMED INSTANCES you can use this:
```
DB_HOST="127.0.0.1"
DB_USERNAME="sa"
DB_PASSWORD="YOUR_PASSWORD"
DB_DIALECT="mssql"
DB_INSTANCE_NAME="SQLEXPRESS"
```



## Building C++ SilkroadSecurity
- Install Visual Studio or Build Tools 2022 with C++ desktop stack.
- Install boost 1_88_0 from https://www.boost.org/users/history/version_1_80_0.html
- Extract the package in your suitable directory and run `bootstrap.bat` from an elevated shell (you might need to set the `--msvs_version=2022` attribute)
- Add environment variable (if not added) `INCLUDE=C:\dev\boost` - you can use your own path for that.
- When above finished run `.\b2` from same path and you should be good to go.

You'll need to adjust `binding.gyp` in `silkroad-security` folder to similar as below (change path to your own boost installation):

```json
{
  "targets": [
    {
      "target_name": "SilkroadSecurityJS",
      "cflags!": [ "-fno-exceptions" ],
      "cflags_cc!": [ "-fno-exceptions" ],
      "sources": [
        "./src/SilkroadSecurity/blowfish.cpp",
        "./src/SilkroadSecurity/silkroad_security.cpp",
        "./src/SilkroadSecurity/stream_utility.cpp",

        "./src/SilkroadSecurityJS.cpp",
        "./src/main.cpp"
      ],
      "include_dirs": [
        "<!@(node -p \"require('node-addon-api').include\")",
        "C:/dev/boost_1_80_0"
      ],
      "libraries": [
        "C:/dev/boost_1_80_0/stage/lib/libboost_random-vc143-mt-s-x64-1_80.lib"
      ],
      "defines": [ "NAPI_DISABLE_CPP_EXCEPTIONS" ],
    }
  ]
}
```

## What else?
- Packet Docs and Silkroad API to's https://github.com/DummkopfOfHachtenduden/SilkroadDoc
- SilkroadSecurity by @EmirAzaiez: https://github.com/EmirAzaiez/SilkroadSecurityJS
