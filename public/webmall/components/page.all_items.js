var page_all_items = function (trx) {
  var container = $(div);

  // categories tabs:
  var renderTabs = function () {
    var tabContainer = $(div).addClass("tabs");

    $(app.state.categories).each(function (index, category) {
      var tab = $(div)
        .html(category["shop_name_" + app.state.loc])
        .addClass("tab");

      if (index === app.state.selected.category) {
        tab.addClass("active");
      } else {
        tab.on("click", function () {
          app.state.selected.category = index;
          render();
        });
      }

      tabContainer.append(tab);
    });

    container.append(tabContainer);
  };

  // categories sub headers + items:
  var renderCategory = function () {
    var subCategories = app.state.categories[app.state.selected.category].subCategories;
    var scrollFrame = $(div).addClass("categories");

    // Categories:
    $(subCategories).each(function (index, category) {
      var wrapper = $(div).addClass("category");
      // Category items:
      if (category.items.length > 0) {
        var header = $(div).addClass("header");
        var itemWrapper = $(div).addClass("items").hide();

        $(category.items).each(function (_, item) {
          var itemContainer = $(div).addClass("item listing");
          var imageTitleContainer = $(div).addClass("title-container");
          var priceContainer = $(div).addClass("price-container");
          var buttonsContainer = $(div).addClass("buttons-container");
          var item_name = item.detail ? item.detail.item_name_eng : "n/a";
          var title = $(div).addClass("title").html(item_name);

          var buyButton = $(button).addClass("purchase").html(trx.purchase);
          var cartButton = $(button).addClass("cart");

          var price_class = {
            0: "silk_own",
            3: "silk_own_premium",
            5: "silk_point",
          };

          var selected_silk = price_class[app.state.selected.silk_type];
          var price_trx = trx[selected_silk];

          var price = $(div)
            .addClass("price")
            .html(item.silk_price + " "+ price_trx);

         

          price.addClass(price_class[item.silk_type]);

          var image = $(img, {
            id: item.package_code,
            class: "item-icon",
            src: "/images/items/" + item.package_code + ".jpg",
            alt: item_name,
          });

          var special_icon = false;
          var preview_image = false;

          // purchase:
          buyButton.on("click", function () {
            purchase(trx, [{item: item, qty: 1}]);
          });

          // add to cart:
          cartButton.on("click", function () {
            cart_add(trx, item);
          });

          if (item.package_code.match("COS|PET2|AVATAR|BOOTH") && !item.package_code.match("NASRUN")) {
            preview_image = $('<img src="/images/items/' + item.package_code + '_detail.jpg" />')
              .addClass("preview")
              .load()
              .error(function () {
                $(this).addClass("hidden");
              });
          }

          imageTitleContainer.append(image);
          imageTitleContainer.append(title);

          imageTitleContainer.hover(function (event) {
            if (event.type === "mouseleave") {
              $(".tooltip").remove();
            } else {
              var tooltip = $(div).addClass("tooltip");

              if (!$(preview_image).hasClass("hidden")) {
                tooltip.append(preview_image);
              }

              tooltip
                .append($("<h3>" + item.lang["package_name"] + "</h3>"))
                .append($("<b>Description:</b><br />"))
                .append($("<span>" + item.lang[app.state.loc + "_explain"] + "</span><br />"))
                .append($("<b>Usage:</b><br />"))
                .append($("<span>" + item.lang[app.state.loc + "_use_method"] + "</span><br />"))
                .append($("<b>Restrictions:</b><br />"))
                .append($("<span>" + item.lang[app.state.loc + "_use_restriction"] + "</span><br />"))
                .appendTo(this)
                .css("top", event.pageY + 10 + "px")
                .css("left", event.pageX - 30 + "px")
                .fadeIn();
            }
          });

          imageTitleContainer.mousemove(function (event) {
            $(".tooltip")
              .css("top", event.pageY + 10 + "px")
              .css("left", event.pageX - 30 + "px");
          });

          // new, best, sale icons:
          if (item.mall) {
            if (item.mall.is_new) {
              special_icon = $(img).attr("src", "/images/item_new_icon.png");
            }
            if (item.mall.is_best) {
              special_icon = $(img).attr("src", "/images/item_best_icon.png");
            }
            if (item.mall.is_list) {
              special_icon = $(img).attr("src", "/images/item_sale_icon.png");
            }
          }

          priceContainer.append(price);
          buttonsContainer.append(buyButton);
          buttonsContainer.append(cartButton);

          itemContainer.append(imageTitleContainer);
          itemContainer.append(priceContainer);
          itemContainer.append(buttonsContainer);

          if (special_icon) {
            var special_frame = $(div).addClass("special").append(special_icon);
            itemContainer.append(special_frame);
          }

          itemWrapper.append(itemContainer);
        });

        header.append(category["sub_name_" + app.state.loc]);

        header.on("click", function () {
          var same_element = $(this).hasClass("active");

          $(".items").hide();

          $(".category > .header").each(function () {
            $(this).removeClass("active");
          });

          if (!same_element) {
            $(this).addClass("active");

            scrollFrame.scrollTop();
            itemWrapper.fadeIn();
          }
        });

        wrapper.append(header);
        wrapper.append(itemWrapper);
        scrollFrame.append(wrapper);
      }
    });

    var first_section = $(scrollFrame.children()[0]);
    var first_section_header = $(first_section.children()[0]);
    var first_section_items = $(first_section.children()[1]);

    first_section_header.addClass("active");
    first_section_items.fadeIn();

    container.append(scrollFrame);
  };

  var render = function () {
    if (app.state.categories.length > 0) {
      container.empty();
      renderTabs();
      renderCategory();
    }
  };

  // preload & sort data chunks (if none):
  if (!app.state.initial_categories) {
    load_categories(function () {
      render();
    });
  } else {
    render();
  }

  $(container).append(loading(trx));

  return {component: container, render: render};
};
