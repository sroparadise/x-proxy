var session_interval = false;
var silk_fetch_timeout = 1000; // 2s

var silk_balance = function (trx) {
  var container = $(div).addClass("box");
  var title = $(div).addClass("title").append(trx.account_balance);
  var body = $(div).addClass("body");

  if (session_interval) clearTimeout(session_interval);

  var render = function () {
    var silk_types = app.state.silk_types;
    var wallet = app.state.wallet;

    body.empty();

    $(silk_types).each(function (index, item) {
      var wrap = $(div).addClass("values");
      var title = $(div).addClass("label");
      var icon = $(img).attr("src", item.icon);
      var value = $(div).addClass("value");

      title.append(item.name);
      value.append(icon).append(" ").append(parseInt(wallet[item.key]));
      wrap.append(value).append(" ").append(title);
      body.append(wrap);
    });

    container.append(title).append(body);

    session_interval = setTimeout(function () {
      $.get("/webmall/session?jid=" + app.state.jid + "&key=" + app.state.key, function (update) {
        app.state.wallet = update.wallet;
        render();
      });
    }, silk_fetch_timeout);

  };

  render();

  return container;
};
