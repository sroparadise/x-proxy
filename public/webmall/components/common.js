var notificationTimeout;

var price_class = {
	0: 'silk_own',
	3: 'silk_own_premium',
	5: 'silk_point',
};

var hook_reload = function (trx) {
	var container = $(div).addClass('refresh-container');

	var control = $(button).addClass('refresh').text(trx.reload);

	control.on('click', function () {
		window.location.reload();
	});

	container.append(control);

	return container;
};

// trigger notice:
var notification = function (message, timeout) {
	var container = $(div).addClass('notice');

	container.append(message);
	app.container.append(container);
	container.fadeIn();

	timeout = timeout ? timeout : 3000;

	notificationTimeout = setTimeout(function () {
		container.fadeOut('fast');

		setTimeout(function () {
			container.remove();
		}, 1000);
	}, timeout);
};

// loading element
var loading = function (trx) {
	var container = $(div).addClass('loading');
	var message = $(div).append(trx.loading);
	var image = $(img).attr('src', '/images/loader.gif');

	container.append(image);
	container.append(message);

	return container;
};

var modal = function (trx, title, content, size) {
	var container = $(div).addClass('modal');
	var modalWindow = $(div).addClass('window').addClass(size);
	var closeWindow = $(div).addClass('close');
	var header = $(div).addClass('header');
	var titleWrapper = $(div).addClass('title');

	var open = function () {
		app.container.append(container);
		modalWindow.fadeIn();
	};

	var close = function () {
		modalWindow.fadeOut();
		setTimeout(function () {
			container.remove();
		}, 500);
	};

	closeWindow.on('click', close);

	titleWrapper.text(title);
	header.append(titleWrapper);
	header.append(closeWindow);
	modalWindow.append(header);
	modalWindow.append(content);
	container.append(modalWindow);

	return {open: open, close: close, window: modalWindow};
};

var purchase = function (trx, items) {
	var container = $(div).addClass('body');
	var buyButton = $(button).addClass('purchase').html(trx.confirm_purchase);
	var cartButton = $(button).addClass('cart');
	var buttonsContainer = $(div).addClass('buttons-container');
	var modal_instance = modal(trx, trx.purchase_item, container, 'small');
	var itemWrapper = $(div).addClass('items').hide();
	var sub_total = $(div).addClass('subtotal');
	var total_cost = 0;
	var selected_silk = price_class[app.state.selected.silk_type];
	var price_trx = trx[selected_silk];

	$(items).each(function (index, data) {
		var quantity = data.qty;
		var item = data.item;
		var itemContainer = $(div).addClass('item');
		var imageInfoContainer = $(div).addClass('title-container');
		var priceContainer = $(div).addClass('price-container');
		var item_name = item.detail ? item.detail.item_name_eng : 'n/a';
		var info = $(div).addClass('info');
		var info_text = $(div).html(item.lang[app.state.loc + '_use_method']);
		var buttonAdd = $(button).addClass('control').text('+');
		var buttonRemove = $(button).addClass('control').text('-');
		var container = $(div).addClass('x');
		var qtyContainer = $(div).addClass('qty');
		var qtyArea = $(b);
		var price = $(div).addClass('price');
		var preview_image = false;
		var image = $(img, {
			id: item.package_code,
			class: 'item-icon',
			src: '/images/items/' + item.package_code + '.jpg',
			alt: item_name,
		});

		var total = $(span).addClass('total');

		var render_total = function () {
			total.text(' = ' + item.silk_price * quantity + ' ' + trx.total);
			sub_total.empty().append('<b>' + trx.subtotal + ':</b> <span class="totalcost">' + total_cost + ' ' + price_trx + '</span>');
		};

		var render_quantity = function () {
			qtyArea.text(quantity);
		};

		var image_wrap = $(div).append(image);

		if (item.package_code.match('COS|PET2|AVATAR|BOOTH') && !item.package_code.match('NASRUN')) {
			preview_image = $('<img src="/images/items/' + item.package_code + '_detail.jpg" />')
				.addClass('preview')
				.load()
				.error(function () {
					$(this).addClass('hidden');
				});
		}

		price.html(item.silk_price + ' ' + price_trx);

		buttonAdd.on('click', function () {
			if (total_cost + item.silk_price <= app.state.wallet[selected_silk]) {
				buttonRemove.removeClass('bad');
				buttonAdd.removeClass('bad');
				quantity++;
				items[index].qty = quantity;
				total_cost += item.silk_price;
				render_total();
				render_quantity();
			} else {
				buttonAdd.addClass('bad');
			}
		});

		buttonRemove.on('click', function () {
			if (quantity > 1) {
				buttonRemove.removeClass('bad');
				buttonAdd.removeClass('bad');
				quantity--;
				items[index].qty = quantity;
				total_cost -= item.silk_price;
				render_total();
				render_quantity();
			} else {
				buttonRemove.addClass('bad');
			}
		});

		price.addClass(price_class[item.silk_type]);

		qtyContainer.append(buttonRemove);
		qtyContainer.append(' ');
		qtyContainer.append(qtyArea);
		qtyContainer.append(' ');
		qtyContainer.append(buttonAdd);
		qtyContainer.append(total);

		priceContainer.append(price);

		info.append(priceContainer);

		info.append(qtyContainer);

		image_wrap.hover(function (event) {
			if (event.type === 'mouseleave') {
				$('.tooltip').remove();
			} else {
				var tooltip = $(div).addClass('tooltip');

				if (!$(preview_image).hasClass('hidden')) {
					tooltip.append(preview_image);
				}

				tooltip
					.append($('<h3>' + item.lang['package_name'] + '</h3>'))
					.append($('<b>Description:</b><br />'))
					.append($('<span>' + item.lang[app.state.loc + '_explain'] + '</span><br />'))
					.append($('<b>Usage:</b><br />'))
					.append($('<span>' + item.lang[app.state.loc + '_use_method'] + '</span><br />'))
					.append($('<b>Restrictions:</b><br />'))
					.append($('<span>' + item.lang[app.state.loc + '_use_restriction'] + '</span><br />'))
					.appendTo(this)
					.css('top', event.pageY + 10 + 'px')
					.css('left', event.pageX - 30 + 'px')
					.fadeIn();
			}
		});

		image_wrap.mousemove(function (event) {
			$('.tooltip')
				.css('top', event.pageY + 10 + 'px')
				.css('left', event.pageX - 30 + 'px');
		});

		imageInfoContainer.append(image_wrap);

		imageInfoContainer.append(info);

		itemContainer.append(imageInfoContainer);

		itemWrapper.append(itemContainer);

		total_cost += item.silk_price;

		render_total();
		render_quantity();
	});

	buyButton.on('click', function () {
		var selected_silk = price_class[app.state.selected.silk_type];
		var cart_query = _.map(items, function (i, key) {
			return {
				package: i.item.package_id,
				qty: i.qty,
			};
		});

		$.get('/webmall/purchase?jid=' + app.state.jid + '&key=' + app.state.key + '&items=' + JSON.stringify(cart_query)).then(function (purchase_result) {
			modal_instance.close();
			if (purchase_result.status) {
				var success_container = $(div).addClass("success");
				var success_image = $(img).attr("src", "/images/content_rpitems_img.gif");
				var success_header = "<h3>"+trx.purchase_success_great+"</h3>";
				var success_guide = $(div).append(trx.purchase_success_guide);
				
				success_container.append(success_header);
				success_container.append(success_guide);
				success_container.append(success_image);

				notification(success_container, 4000);
			} else {
				notification('<span class="error">Failed to purchase!</span>', 2000);
			}
		});
	});

	cartButton.on('click', function () {
		var cart_query = _.map(items, function (i, key) {
			return {
				package: i.item.package_id,
				qty: i.qty,
			};
		});
	});

	itemWrapper.append(sub_total);
	itemWrapper.append(buttonsContainer);

	buttonsContainer.append(buyButton);
	buttonsContainer.append(cartButton);

	modal_instance.open();
	container.append(itemWrapper);
	itemWrapper.fadeIn();
};

var cart_add = function (trx, item) {
	notification(item.package_id);
};

var load_categories = function (callback) {
	var state = {
		total_categories: 0,
		processed_categories: 0,
	};

	$.get('/webmall/categories', function (data) {
		if (data) {
			state.total_categories = data.total;

			$(data.results).each(function (_, category) {
				var queries = [];
				var category_total_items = 0;

				$(category.subCategories).each(function (sub_index, sub) {
					category.subCategories[sub_index].items = [];

					// items under subcategory:
					queries.push($.get('/webmall/items?shop_no=' + sub.ref_no + '&shop_no_sub=' + sub.sub_no + '&silk_type=' + app.state.selected.silk_type));

					// workaround for dumb array case:
					queries.push(false);
				});

				// make sure all sub category items are loaded:
				$.when.apply($, queries).then(function () {
					var _r_index = 0;

					$(arguments).each(function (_, result) {
						if (result) {
							category.subCategories[_r_index].items = result[0].results;
							category_total_items += result[0].total;

							_r_index++;
						}
					});

					if (category_total_items > 0) {
						app.state.categories.push(category);
					}

					state.processed_categories++;

					if (state.processed_categories === state.total_categories) {
						app.state.initial_categories = JSON.stringify(app.state.categories);
						callback();
					}
				});
			});
		}
	});
};

// instant search:
var filter_categories = function (value) {
	return _.reduce(
		app.state.categories,
		function (arr, category) {
			var name_key = 'shop_name_' + app.state.loc;
			var match_category = category[name_key].toLowerCase().indexOf(value) > -1;

			var subCategories = _.reduce(
				category.subCategories,
				function (s_arr, subCategory) {
					//  matching subCategory
					var s_name_key = 'sub_name_' + app.state.loc;
					var match_sub_category = subCategory[s_name_key].toLowerCase().indexOf(value) > -1;

					var items = _.reduce(
						subCategory.items,
						function (i_arr, item) {
							switch (true) {
								// Matching values cases:
								case item.lang.package_name.toLowerCase().indexOf(value) > -1:
								case item.detail.item_name.toLowerCase().indexOf(value) > -1:
								case item.detail.item_name_eng.toLowerCase().indexOf(value) > -1:
								case item.lang[app.state.loc + '_explain'].toLowerCase().indexOf(value) > -1:
								case item.lang[app.state.loc + '_use_method'].toLowerCase().indexOf(value) > -1:
								case item.lang[app.state.loc + '_use_restriction'].toLowerCase().indexOf(value) > -1:
									i_arr.push(item);
									break;
								//
								default:
									break;
							}

							return i_arr;
						},
						new Array()
					);

					if (items.length > 0) {
						match_sub_category = true;
						subCategory.items = items;
					}

					if (match_sub_category) {
						s_arr.push(subCategory);
					}

					return s_arr;
				},
				new Array()
			);

			if (subCategories.length > 0) {
				match_category = true;
				category.subCategories = subCategories;
			}

			if (match_category) {
				arr.push(category);
			}

			return arr;
		},
		new Array()
	);
};
