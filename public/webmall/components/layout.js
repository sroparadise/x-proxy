var layout = function (trx, renderItems) {
  var container = $(div).addClass("wrapper");
  var aside = $(div).addClass("aside");
  var pageContainer = $(div).addClass("content");
  var page = app.state.sections[app.state.selected.section];

  // load side components:
  aside.append(silk_balance(trx));
  aside.append(silk_switcher(trx));
  aside.append(search(trx, renderItems));

  pageContainer.append(page.component);

  container.append(aside);
  container.append(pageContainer);

  return container;
};
