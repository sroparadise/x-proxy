var silk_switcher = function (trx) {
  var container = $(div).addClass("box");
  var title = $(div).addClass("title").append(trx.title_balance_switch);
  var body = $(div).addClass("body").addClass("select-silk");
  var silk_types = app.state.silk_types;

  $(silk_types).each(function (index, item) {
    var control = $(label);
    var radio = $(input).attr("type", "radio").attr("name", "silk_type").attr("value", item.id);
    var icon = $(img).attr("src", item.icon);
    var _index = index + 1;

    if (item.id === app.state.selected.silk_type) {
      radio.attr("checked", "checked");
    } else {
      radio.on("change", function () {
        app.state.selected.category = 0;
        app.state.categories = [];
        app.state.initial_categories = false;
        app.state.selected.silk_type = item.id;
        app.render();
      });
    }

    control.append(radio).append(" ").append(icon).append(" ").append(item.name);
    body.append(control);
  });

  container.append(title).append(body);

  return container;
};
