var navigation = function (trx) {
  var container = $(div).addClass("navbar");
  var username = $(b).append(app.state.username);
  var header = $(div).addClass("header");
  var sections = $(div).addClass("sections");
  var keys = Object.keys(app.state.sections);

  $(keys).each(function (index, key) {
    var section = app.state.sections[key];
    var menu_item = $(div).addClass("button").append(section.name);

    if (key === app.state.selected.section) {
      menu_item.addClass("active");
    } else {
      menu_item.on("click", function () {
        app.state.selected.section = key;
        app.render();
      });
    }

    sections.append(menu_item);
  });

  // Welcome, {username}:
  header.append(trx.welcome);
  header.append(" ");
  header.append(username);
  container.append(header).append(sections);

  return container;
};
