var search = function (trx, render_items) {
  var container = $(div).addClass("box");
  var title = $(div).addClass("title").append(trx.search);
  var body = $(div).addClass("body");
  var controlWrap = $(div).addClass("input");
  var control = $('<input type="text" value="" name="search" placeholder="Enter item name..." />').attr("maxlength", 20);

  var reset = function () {
    app.state.categories = JSON.parse(app.state.initial_categories);
    render_items();
  };

  var set_filtered = function (value) {
    var result = filter_categories(value);
    app.state.selected.category = 0;
    app.state.categories = result;
    render_items();
  };

  control.on("textchange", function () {
    var value = $(this).val().toLowerCase();

    if (!value || value === "" || value === " " || value.length === 0) {
      reset();
    } else {
      set_filtered(value);
    }
  });

  controlWrap.append(control);
  body.append(controlWrap);

  container.append(title).append(body);

  return container;
};
