var app = new AppBase("#app");

app.init(function () {
  // retrieve language file based on `loc` variable:
  $.get("/webmall/translations/" + app.state.loc + ".json", function (trx) {

    app.state.cart_items = [];

    app.state.categories = [];
    app.state.initial_categories = false;
    
    app.state.selected = {
      silk_type: 0,
      section: "all",
      category: 0,
    };

    app.state.silk_types = [
      {
        id: 0,
        key: "silk_own",
        icon: "/images/silk.gif",
        name: trx.silk_own,
      },
      {
        id: 3,
        key: "silk_own_premium",
        icon: "/images/silk_premium.gif",
        name: trx.silk_own_premium,
      },
      {
        id: 5,
        key: "silk_point",
        icon: "/images/mall_silk_icon.gif",
        name: trx.silk_point,
      },
    ];

    app.render = function () {
      var all_items = page_all_items(trx);

      app.state.sections = {
        all: {
          name: trx.all_items,
          component: all_items.component,
        },
        reserved: {
          name: trx.reserved,
          component: page_reserved(trx),
        },
        history: {
          name: trx.history,
          component: page_history(trx),
        },
      };

      app.container.empty();
      app.container.append(navigation(trx));
      app.container.append(layout(trx, all_items.render));
      app.container.append(hook_reload(trx));
    };

    app.render();
  });
});
