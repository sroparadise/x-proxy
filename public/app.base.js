var div = "<div />";
var b = "<b />"; 
var p = "<p />";
var label = "<label />";
var input = "<input />";
var img = "<img />";
var button = "<button />";
var span = "<span />";

// Otherwise silk will not update, seems like the browser is modified custom not just ie9
$.ajaxSetup({
  cache: false,
});

// App prototype;
var AppBase = function (container) {
  this.state = {};
  this.container = container;
};

AppBase.prototype.getSession = function () {
  var container = $("#session").attr("token");
  var split = Base64.decode(container).split("|");

  return {
    jid: split[0],
    key: split[1],
    loc: split[2],
    username: split[3],
    wallet: {
      silk_own: split[4],
      silk_own_premium: split[5],
      silk_point: split[6],
    },
  };
};

AppBase.prototype.init = function (callback) {
  this.container = $(this.container);
  this.state = this.getSession();
  callback(this);
};
