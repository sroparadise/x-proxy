module.exports = {
  up: ({schema, fn}) =>
    schema.createTable("achievements", t => {
      t.increments("id");

      t.integer("UserJID").notNullable();
      t.integer("CharID").notNullable();

      t.string("type").notNullable();
      t.enum("status", ["pending", "completed", "cancelled"]);

      t.json("meta");

      t.timestamp("created").defaultTo(fn.now());
      t.timestamp("updated").defaultTo(fn.now());
    }),
  down: ({schema}) => schema.dropTable("achievements"),
};
