module.exports = {
	up: ({schema, fn}) =>
		schema.createTable('api_tokens', (t) => {
			t.increments('id');

			t.integer('UserJID');
			t.string('token');
            t.string('service').defaultTo('discord');

			t.timestamp('created').defaultTo(fn.now());
			t.timestamp('updated').defaultTo(fn.now());
		}),
	down: ({schema}) => schema.dropTable('api_tokens'),
};
