module.exports = {
	up: ({schema, fn}) =>
		schema.createTable('api_tokens_link', (t) => {
			t.increments('id');

			t.integer('token_id').notNullable();
            t.string('service_id').notNullable();

			t.timestamp('created').defaultTo(fn.now());
			t.timestamp('updated').defaultTo(fn.now());
		}),
	down: ({schema}) => schema.dropTable('api_tokens_link'),
};
