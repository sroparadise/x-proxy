module.exports = {
  up: ({schema, fn}) =>
    schema.createTable("chat_logs", t => {
      t.increments("id");

      t.integer("UserJID").notNullable();
      t.integer("CharID").notNullable();

      t.string("type").notNullable();

      t.string("message").notNullable();

      t.string("receiver");

      t.timestamp("created").defaultTo(fn.now());
      t.timestamp("updated").defaultTo(fn.now());
    }),
  down: ({schema}) => schema.dropTable("chat_logs"),
};
