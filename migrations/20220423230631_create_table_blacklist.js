module.exports = {
    up: ({schema, fn}) =>
      schema.createTable("blacklist", t => {
        t.increments("id");
  
        t.string("remote").notNullable();
        t.string("reason");
  
        t.timestamp("created").defaultTo(fn.now());
        t.timestamp("updated").defaultTo(fn.now());
      }),
    down: ({schema}) => schema.dropTable("blacklist"),
  };
  