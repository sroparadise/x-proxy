import axios from 'axios';

const {API_HOST} = process.env;

export default async function handler(req, res) {
	if (req.method === 'POST') {
		const {
			body: {connections},
		} = req;

		const keys = Object.keys(connections);

		for await (const key of keys) {
			await axios.post(`${API_HOST}/publish`, {
				context: key,
				operation: 'disconnect',
				data: connections[key],
			});
		}

		res.status(200).end();
	} else {
		res.status(404).end();
	}
}
