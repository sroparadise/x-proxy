import axios from 'axios';

const {API_HOST} = process.env;

export default async function handler(req, res) {
	if (req.method === 'POST') {
		const {
			body: {connections, payload},
		} = req;

		const keys = Object.keys(connections);

		for await (const key of keys) {
			await axios.post(`${API_HOST}/publish`, {
				context: key,
				operation: 'message',
				data: {
					direction: payload.direction,
					targets: connections[key],
					opcode: payload?.opcode || false,
					encrypted: payload?.encrypted || false,
					massive: payload?.massive || false,
					data: payload.buffer,
				},
			});
		}

		res.status(200).end();
	} else {
		res.status(404).end();
	}
}
