import * as React from 'react';
import Divider from '@mui/material/Divider';
import Drawer from '@mui/material/Drawer';
import List from '@mui/material/List';
import Box from '@mui/material/Box';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import PeopleIcon from '@mui/icons-material/People';
import TimerIcon from '@mui/icons-material/Timer';
import SettingsIcon from '@mui/icons-material/Settings';
import Image from 'next/image';
import {Globe, Tool} from 'react-feather';
import {
	BlockSharp,
	Dashboard,
	Payments,
	PeopleTwoTone,
	ShoppingCartCheckout,
} from '@mui/icons-material';
import {useRouter} from 'next/router';
import Link from 'next/link';
import FadeIn from './FadeIn';

const categories = [
	{
		id: 'Management',
		children: [
			{
				id: 'Connections',
				icon: <Globe />,
				url: '/connections',
			},
			{
				id: 'Utilities',
				icon: <Tool />,
				url: '/utilities',
			},
			{
				id: 'Purchases',
				icon: <Payments />,
				url: '/purchases',
			},
			{
				id: 'Accounts',
				icon: <PeopleIcon />,
				url: '/accounts',
			},
			{
				id: 'Characters',
				icon: <PeopleTwoTone />,
				url: '/characters',
			},
			{
				id: 'Restrictions',
				icon: <BlockSharp />,
				url: '/restrictions',
			},
		],
	},
	{
		id: 'Configuration',
		children: [
			{id: 'Proxy Settings', icon: <SettingsIcon />, url: '/settings/proxy'},
			{id: 'Auto Events', icon: <TimerIcon />, url: '/settings/events'},
			{id: 'Webmall Editor', icon: <ShoppingCartCheckout />, url: '/settings/webmall'},
		],
	},
];

const item = {
	color: 'rgba(255, 255, 255, 0.7)',
	'&:hover, &:focus': {
		bgcolor: 'rgba(255, 255, 255, 0.08)',
	},
};

const itemCategory = {
	boxShadow: '0 -1px 0 rgb(255,255,255,0.1) inset',
};

function Navigation(props) {
	const {...other} = props;
	const router = useRouter();

	console.log(router);

	return (
		<Drawer variant="permanent" {...other}>
			<List disablePadding>
				<ListItem
					sx={{
						...item,
						...itemCategory,
						display: 'flex',
						alignItems: 'center',
						justifyContent: 'center',
					}}
				>
					<Image
						className="background"
						src={`/logo.png`}
						priority
						width={120}
						height={120}
						alt={`SRO Suite`}
					/>
				</ListItem>
				<ListItem disablePadding sx={{...item, ...itemCategory}}>
					<ListItemButton href={'/'} LinkComponent={Link} selected={router.asPath === '/'}>
						<ListItemIcon>
							<Dashboard />
						</ListItemIcon>
						<ListItemText>Dashboard</ListItemText>
					</ListItemButton>
				</ListItem>
				{categories.map(({id, children}) => (
					<Box key={id}>
						<ListItem sx={{py: 2, px: 3}}>
							<ListItemText sx={{color: '#fff'}}>{id}</ListItemText>
						</ListItem>
						{children.map(({id: childId, icon, url}, idx) => (
							<FadeIn key={childId} delay={idx * 50}>
								<ListItem disablePadding>
								<ListItemButton
									LinkComponent={Link}
									href={url}
									selected={router.asPath.startsWith(url)}
									sx={item}
								>
									<ListItemIcon>{icon}</ListItemIcon>
									<ListItemText>{childId}</ListItemText>
								</ListItemButton>
							</ListItem>
							</FadeIn>
						))}

						<Divider sx={{mt: 2}} />
					</Box>
				))}
			</List>
		</Drawer>
	);
}

export default Navigation;
