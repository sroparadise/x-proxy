# `Quick` guide to start writing your own features in sro-suite
Let's consider that we'd like to do something when an opcode `0x7001` (Select Character) from client hits the server, in my case I will simply get the characters data from DataServer and store it in proxy temporary memory for later use in other places.

- First we need a controller file - see it in `src/lib/Proxy/controllers` file - `CharacterSelect.js`
- A classic controller structure will be as following: 
```js
export default async (packet, instance, config) => {
    // do stuff here

    // return the original packet to it's original direction:
    return [packet];
};
```
- As you can see the controller has 3 variables `packet`, `instance` and `config`
    - `packet` is the incoming packet data to the proxy
        - A packet requires a schema if you want it to be processed automatically into developer-friendly data - see `src/lib/Proxy/schemas/CharacterSelect.js` for this case
        - After building a schema you can now do this:
        ```js
        import {Reader} from '@lib/utils/security'; // import Reader utility from silkroad-security
        import {schema} from '@lib/Proxy/schemas/CharacterSelect'; // Import the packet schema file

        const read = new Reader(packet.data); // Pass the packet.data to reader
	    const data = read.fromSchema(schema); // Process the data according to the schema
        
        // data => {CharName16: "johny"} 
        ```
        With this we've successfully retrieved the character name from the incoming buffer in a JSON format.
        - Now we'd want to lookup the character data from database:
        ```js
        import database from '@lib/utils/database'; // import database plugin

        const {
            data: {
                results: [
                    {
                        CharID,
                        CharName16,
                        characterLink: {
                            user: {JID: UserJID, StrUserID, sec_primary, sec_content}, // Variables from database we want
                        },
                    },
                ],
            },
        } = await database({
            method: 'POST', 
            url: '/characters/search', // Search the "characters" model
            data: {
                query: {
                    characterLink: { // also attach the user related to this character
                        user: {},
                    },
                    $where: {
                        CharName16: data.CharName16, // where charname16 matches the data.CharName16 (from packet schema)
                    },
                },
                fields: [ // Fields/columns that we would like to select:
                    'CharID',
                    'characterLink.user.JID',
                    'characterLink.user.StrUserID',
                    'characterLink.user.StrEmail',
                    'characterLink.user.sec_primary',
                    'characterLink.user.sec_content',
                ],
            },
        });
        ```
        - Remember the data on the Session:
        ```js
        const memory_init = {
            CharName16,
            CharID,
            UserJID,
            sec_primary,
            sec_content,
            StrUserID,
        };

        // Set temporary memory variables:
        Object.keys(memory_init).forEach((key) => instance.memory.set(key, memory_init[key]));

        // add an database entry for connections list for this user/character:
        await database({
            method: 'PUT',
            url: '/connections',
            data: {
                where: {
                    connection_id: instance.id,
                    context: config.module_alias,
                },
                payload: {
                    CharID,
                    UserJID,
                },
            },
        });
        ```
    - `instance` is the currently connected session that this applies on (do `console.log(instance)` to understand what's in there or see Session.js in Proxy)
    - `config` is the module configuration (AgentServer.js from config folder)

- We also import and export this controller in `src/lib/Proxy/controllers/index.js` file for it to be available from namespace.
- To enable this controller we will need to add it to the right module configuration - this one goes in `src/config/AgentServer.js` as following:
```js
import {CharacterSelect} from '@controllers';
...
controllers: {
    client: {
        0x7001: CharacterSelect, <--- This is the moment we have hooked it up to a client -> server opcode 
        ...
    },
    remote: {},
},
...
```

Not that simple, eh.

---
## Let's say we would like to override launcher news packet with our own news at proxy level:
```js
import {Reader, Writer} from '@lib/utils/security';
import {version, name, repository} from '@root/package.json';
import {schema} from '@lib/Proxy/schemas/LauncherNews'; // see the file for schema layout

export default async (packet) => {
	// process original packet:
	const read = new Reader(packet.data);

	// read the actual packet via schema:
	const data = read.fromSchema(schema);

	const posts = [
		// inject some custom news:
		{
			subject: `${name.toUpperCase()}`,
			article: `<b>Version:</b> ${version}<br>[<a href="${repository.url}" ><font color=brown>View on Gitlab</font></a>]`,
			year: 2022,
			month: 12,
			day: 12,
			hour: 12,
			minute: 59,
			second: 59,
			ms: 130000000,
		},
		...data.posts, // append any previous news from original packet
	];

	const writer = new Writer();

	const buffer = writer.fromSchema(schema, {
		count: posts.length,
		posts,
	});

	return [{...packet, data: buffer}];
};
```

