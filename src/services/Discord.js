import config from '@config/DiscordBot';
import DiscordBot from '@lib/DiscordBot';

class Discord extends DiscordBot {
	constructor() {
		super(config);
	}
}

export default Discord;
