import config from '@config/WebApps';
import WebAppServer from '@lib/AppServer';
import webmall from '@lib/AppServer/routes/webmall';
import billing from '@lib/AppServer/routes/billing';
import ranking from '@lib/AppServer/routes/ranking';
import filePreview from '@lib/AppServer/routes/file';
import survey from '@lib/AppServer/routes/survey';

class WebApps extends WebAppServer {
	constructor() {
		super(config);

		// CHECKUSER.aspx replacement
		this.server.get(`/auth`, billing.index);

		// WEBMALL:
		this.server.get('/webmall', webmall.index);
		this.server.get('/webmall/categories', webmall.api_categories);
		this.server.get('/webmall/session', webmall.api_query_session);
		this.server.get('/webmall/items', webmall.api_items);
		this.server.get('/webmall/purchase', webmall.api_items_purchase);

		// RANKING:
		this.server.get(`/ranking`, ranking.index);

		// SURVEY:
		this.server.get(`/survey`, survey.index);

		//  DO NOT EXPOSE THIS TO PUBLIC:
		if (process.env.NODE_ENV !== 'production') {
			this.server.get('/preview', filePreview.index);
			this.server.get('/preview/file', filePreview.loadFile);
		}
	}
}

export default WebApps;
