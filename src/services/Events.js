import config from '@config/PubSub';
import EventsManager from '@lib/EventsManager';

class Events extends EventsManager {
	constructor() {
		super(config.module);
	}
}

export default Events;
