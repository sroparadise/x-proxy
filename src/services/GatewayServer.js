import config from '@config/GatewayServer';
import Server from '@lib/Proxy/Server';

class Gateway extends Server {
	constructor() {
		super(config);
	}
}

export default Gateway;
