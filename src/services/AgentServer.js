import config from '@config/AgentServer';
import Server from '@lib/Proxy/Server';

class AgentServer extends Server {
	constructor() {
		super(config);
	}
}

export default AgentServer;
