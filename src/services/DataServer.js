import DataServer from '@lib/DataServer';
import config from '@config/DataServer';

class Server extends DataServer {
	constructor() {
		super({
			port: config.port,
			host: '0.0.0.0',
			logger: config.development,
		});
	}
}

export default Server;
