import md5 from 'md5';
import crud from '@lib/DataServer/CRUD';
import User from '@lib/DataServer/models/User';
import WebItemCertifyKey from '@lib/DataServer/models/WebItemCertifyKey';
import config from '@config/WebApps';

async function getSession(jid, key) {
	if (!jid || !key) throw 'INVALID_PAYLOAD';

	const user_model = new crud(User);
	const certify_model = new crud(WebItemCertifyKey);

	const [
		{
			results: [user],
		},
		{
			results: [certify_instance],
		},
	] = await Promise.all([
		// query user:
		user_model.read({
			query: {
				$where: {
					JID: jid,
				},
				wallet: {},
				channel: {},
				connection: {
					character: {},
				},
			},
			fields: [
				'JID',
				'PortalJID',
				'StrUserID',
				'password',
				'passwordSha256',
				'UserIP',
				'wallet.silk_gift',
				'wallet.silk_own',
				'wallet.silk_own_premium',
				'wallet.silk_point',
				'wallet.silk_gift_premium',
			],
		}),
		// Query certifyKey:
		certify_model.read({
			query: {
				$where: {
					UserJID: jid,
				},
			},
			order: 'idx desc',
			limit: 1,
		}),
	]);

	const key_payload = [jid, certify_instance.Certifykey, config.salt].join('');

	const check_key = md5(key_payload).toUpperCase();

	if (check_key !== key) throw new Error('NOT_AUTHORIZED');

	return {
		JID: jid,
		StrUserID: user.StrUserID,
		wallet: user.wallet,
		connection: user.connection,
	};
}

export default getSession;
