import md5 from 'md5';
import crud from '@lib/DataServer/CRUD';
import WebPackageItem from '@models/WebPackageItem';

async function getCategoryItems(
	shop_no,
	shop_no_sub,
	silk_type = 0
) {
	try {
		if (!shop_no || !shop_no_sub) throw 'INVALID_ARGUMENTS';
		const model = new crud(WebPackageItem);

		const {results, total} = await model.read({
			query: {
				$where: {
					service: 1,
					shop_no,
					shop_no_sub,
					silk_type,
				},
				// includes:
				detail: {
					$where: {
						package_id: {
							$exists: true,
						},
					},
				},
				lang: {},
				mall: {
					$where: {
						active: 1,
					},
				},
				preview: {},
			},
		});

		// console.log({results})

		return {results, total};
	} catch (e) {
		// console.log(e);
		return {};
	}
}

export default getCategoryItems;
