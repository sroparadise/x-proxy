import crud from '@lib/DataServer/CRUD';
import WebPackageItem from '@lib/DataServer/models/WebPackageItem';
import WebItemGiveList from '@lib/DataServer/models/WebItemGiveList';
import WebItemGiveListDetail from '@lib/DataServer/models/WebItemGiveListDetail';
import WebItemGiveNotice from '@lib/DataServer/models/WebItemGiveNotice';
import {v4 as uuid} from "uuid";

const silk_index = {
	0: 'silk_own',
	3: 'silk_own_premium',
	5: 'silk_point',
};

/**
 * 
 * @param {*} session - user session.
 * @param {*} items - payload for selected items to be purchased.
 * @returns Status of purchases or error accordingly to the outcome of request.
 */

async function buyItems(session, items) {
	try {
		if (!session.StrUserID || !items.length > 0) throw 'INVALID_PAYLOAD';

		const package_item = new crud(WebPackageItem);

		const meta = await package_item.read({
			query: {
				$where: {
					package_id: {
						$in: items.map((i) => i.package),
					},
					service: 1,
				},
				detail: {},
			},
		});

		if (meta.results.length !== items.length) throw 'LENGTH_MISMATCH';

		let total_costs = {
			silk_own: {
				amount: 0,
				items: [],
			},
			silk_own_premium: {
				amount: 0,
				items: [],
			},
			silk_point: {
				amount: 0,
				items: [],
			},
		};

		for await (const {package: package_id, qty} of items) {
			const {
				silk_price,
				silk_type,
				package_code,
				name_code,
				detail: {item_code},
			} = meta.results.find((i) => i.package_id === package_id);

			const total = qty * silk_price;
			const currency = silk_index[silk_type];

			total_costs[currency].amount += total;
			total_costs[currency].items.push({
				package_code,
				item_code,
				name_code,
				qty,
				silk_price,
			});
		}

		if (
			total_costs.silk_own.amount <= session.wallet.silk_own &&
			total_costs.silk_point.amount <= session.wallet.silk_point &&
			total_costs.silk_own_premium.amount <= session.wallet.silk_own_premium
		) {
			// issue items & deduct silk + create reference for purchases
			const give_list = new crud(WebItemGiveList);
			const give_details = new crud(WebItemGiveListDetail);
			const give_notice = new crud(WebItemGiveNotice);

			const payload_give_list = item => ({
				cp_jid: session.jid,
				shard_id: 64,
				character_id: session?.connection?.CharID,
				character_lv: session?.connection?.character.CurLevel,
				item_code_package: item.package_code,
				item_name_package: item.item_code,
				name_code_package: item.name_code,
				section: 1, 
				silk_own: session.wallet.silk_own,
				silk_own_premium: session.wallet.silk_own_premium,
				silk_gift: 0,
				silk_gift_premium: 0,
				silk_point: session.wallet.silk_point,
				message: '$game',
				reg_ip: session?.connection?.remote,
				reg_date: new Date(),
				recieve_date: new Date(),
				invoice_id: uuid(),
				cp_invoice_id: uuid(),
			});

			if (total_costs.silk_own > 0) {
				// todo
			}

			if (total_costs.silk_own_premium > 0) {
				// todo
			}

			if (total_costs.silk_point > 0) {
				// todo
			}

			return true;
		}

		throw 'INSUFFICIENT_CREDITS';
	} catch (e) {
		throw new Error(e);
	}
}

export default buyItems;
