import md5 from 'md5';
import crud from '@lib/DataServer/CRUD';
import {User} from '@lib/DataServer/models';

async function getUserEmail(username = '', password = '') {
	try {
		if (!username === 0 || !password.length === 0) throw 'INVALID_PAYLOAD';

		const user_model = new crud(User);

		return {user_model};
	} catch (e) {
		throw new Error(e);
	}
}

export default getUserEmail;
