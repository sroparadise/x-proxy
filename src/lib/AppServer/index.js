import fastify from 'fastify';
import {Model} from 'objection';
import knex from 'knex';
import dbConfig from '../DataServer/config';
import path from 'path';
import viewEngine from '@fastify/view';
import staticFilesEngine from '@fastify/static';
import handlebars from 'handlebars';
import logger_instance from '@lib/utils/logger';

const logger = logger_instance('WebApps', 'local');

class WebAppServer_Error extends Error {
	constructor(message = 'UNKNOWN_ERROR') {
		super();
		this.message = message;
		this.statusCode = 404;
	}
}

class WebAppServer {
	constructor(config) {
		this.config = config;
		this.server = fastify(this.config);

		this.database = knex(dbConfig);
		Model.knex(this.database);

		this.server.register(viewEngine, {
			root: path.join(process.cwd(), 'views'),
			engine: {
				handlebars,
			},
		});

		this.server.register(staticFilesEngine, {
			root: path.join(process.cwd(), 'public'),
		});
	}

	async run() {
		try {
			const {port, host} = this.config;
			this.server.setNotFoundHandler((req, res) => {
				res.view('default');
			});
			this.server.listen({port, host});
			this.server.ready((error) => {
				if (error) throw new WebAppServer_Error(error.message);
				logger.log('info', 'server_ready', {host, port, browse: 'http://127.0.0.1'});
			});
		} catch (error) {
			this.server.log.error(error);
			process.exit(1);
		}
	}
}

export default WebAppServer;
