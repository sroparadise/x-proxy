import getSession from '../api/getSession';
import getCategories from '../api/getCategories';
import getCategoryItems from '../api/getCategoryItems';
import purchaseEntities from '../api/purchaseEntities';

const index = async (req, res) => {
	try {
		const {jid, key, loc} = req.query;
		const session = await getSession(jid, key);
		const sessionPayload = Buffer.from(
			[
				jid,
				key,
				loc,
				session.StrUserID,
				session.wallet.silk_own,
				session.wallet.silk_own_premium,
				session.wallet.silk_point,
			].join('|')
		).toString("base64");

		return res.view('webmall', {
			session: {
				token: sessionPayload,
				...session,
			},
		});
	} catch (e) {
		console.log(e)
		return res.view('default');
	}
};

const api_items = async (req, res) => {
	try {
		const {shop_no, shop_no_sub, silk_type} = req.query;

		return await getCategoryItems(
			shop_no,
			shop_no_sub,
			silk_type
		);
	} catch (e) {
		res.view('default');
	}
};

const api_items_purchase = async (req, res) => {
	try {
		const {jid, key, items} = req.query;
		const session = await getSession(jid, key);

		let purchase_query = JSON.parse(items);

		await purchaseEntities(session, purchase_query);

		return {status: "OK"};
	} catch (e) {
		return res.view('default');
	}
};

const api_categories = async (req, res) => {
	try {
		return await getCategories();
	} catch (e) {
		res.view('default');
	}
};

const api_query_session = async (req, res) => {
	try {
		const {jid, key} = req.query;

		return await getSession(jid, key);
	} catch (e) {
		return res.view('default');
	}
};

export default {
	index,
	api_categories,
	api_query_session,
	api_items,
	api_items_purchase,
};
