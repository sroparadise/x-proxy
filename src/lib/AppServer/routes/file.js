import readMesh from "@util/readers/mesh";
import readAnimation from "@util/readers/animation";
import readSkeleton from "@util/readers/skeleton";
import readDungeon from "@util/readers/dungeon";
import readResource from "@util/readers/resource";

const index = async (req, res) => {
	try {
		return res.view('viewer');
	} catch (e) {
		return res.view('default');
	}
};

const loadFile = async (req, res) => {
    try {
        const {path, type} = req.query;

        const index = {
            bms: readMesh,
            ban: readAnimation,
            bsk: readSkeleton,
            dof: readDungeon,
            bsr: readResource,
        };

        return index[type](path);
    } catch (e) {
        return res.view('default');
    }
}

export default {
    index,
    loadFile,
};

