import config from '@config/WebApps';
import md5 from 'md5';
import crud from '@lib/DataServer/CRUD';
import User from '@lib/DataServer/models/User';
import moment from "moment";

const index = async (req, res) => {
	try {
        const user_model = new crud(User);
		const [channel, username, password_hash, ip, unix_timestamp, final_hash] = req.query?.values.split('|');
		const key_payload = `${channel}${username}${password_hash}${ip}${unix_timestamp}${config.salt}`;
		const key = md5(key_payload).toUpperCase();

		if (final_hash !== key) throw "INVALID_KEY_HASH";

        const {
			results: [user],
		} = await user_model.read({
			query: {
				$where: {
					StrUserID: username,
					password: password_hash,
				},
			},
			fields: [
				'JID',
				'PortalJID',
				'StrEmail',
				'Email',
				'EmailCertificationStatus',
				'EmailUniqueStatus',
				'VIPLv',
				'VipExpireTime',
				'VIPUserType',
			],
		});

		const current_timestamp = moment(new Date()).format("YYYY-MM-DD HH:mm:ss");

		if (!user) throw "USER_NOT_FOUND";

		const response_status = 0; //todo

		const payload = [
			response_status,
			user.JID,
			current_timestamp,
			user.EmailCertificationStatus,
			user.EmailUniqueStatus,
			user.StrEmail,
			user.VIPLv,
			moment(user.VipExpireTime || new Date()).format("YYYY-MM-DD HH:mm:ss"),
			user.VIPUserType,
			'AFAILUREDESCRIPTION?',
			'URL-RELATED?',
		].join("|");

		return payload;
	} catch (e) {
		return res.view('default');
	}
};

export default {
	index,
};
