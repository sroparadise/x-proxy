export default {
	params: {
		type: 'object',
		properties: {
			model: {type: 'string'},
		},
		required: ['model'],
	},
	body: {
		type: 'object',
	},
	response: {
		200: {
			type: 'object',
		},
	},
};
