import {port} from '@config/DataServer';

export default {
	swagger: {
		info: {
			title: 'DataServer',
			description: 'DATABASE CRUD SERVICE',
			version: '1.0.0',
		},
		consumes: ['application/json'],
		produces: ['application/json'],
		servers: [
			{
				url: `http://127.0.0.1:6440`,
				description: 'LOCAL',
			},
		],
	},
};
