export default {
	querystring: {
		type: 'object',
		properties: {
			id: {
				type: 'string',
			},
			JID: {
				type: 'string',
			},
			idx: {
				type: 'string',
			},
			UserJID: {
				type: 'string',
			},
			UserJID: {
				type: 'string',
			},
			PortalJID: {
				type: 'string',
			},
			connection_id: {
				type: 'string',
			},
		},
	},
	params: {
		type: 'object',
		properties: {
			model: {type: 'string'},
		},
		required: ['model'],
	},
	response: {
		200: {
			type: 'object',
		},
	},
};
