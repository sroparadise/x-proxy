export default {
	description: 'Search a model and relational data',
	querystring: {
		type: 'object',
		properties: {
			page: {type: 'string'},
			limit: {type: 'string'},
		},
	},
	params: {
		type: 'object',
		properties: {
			model: {type: 'string'},
		},
		required: ['model'],
	},
	body: {
		type: 'object',
		properties: {
			query: {
				type: 'object',
			},
			order: {
				type: 'string'
			},
			fields: {
				type: 'array',
			},
		},
		required: ['query'],
	},
	response: {
		200: {
			type: 'object',
			properties: {
				results: {type: 'array'},
				total: {type: 'number'},
			},
		},
	},
};
