export default {
	body: {
		type: 'object',
		properties: {
			context: {
				type: 'string',
				enum: [
					'AgentServer',
					'GatewayServer',
					'DownloadServer',
					/**
					 * If you ever run more than 100 modules at once increase the integer in below values:
					 */
					// 100 agents
					...[...Array(100).keys()].map((i) => `AgentServer_${i}`),
					// 100 gateways
					...[...Array(100).keys()].map((i) => `GatewayServer_${i}`),
					// 100 downloads
					...[...Array(100).keys()].map((i) => `DownloadServer_${i}`),
				],
			},
			operation: {
				type: 'string',
				enum: ['message', 'disconnect'],
			},
			data: {
				anyOf: [
					{
						type: 'object',
						properties: {
							direction: {
								type: 'string',
								enum: ['client', 'remote'],
							},
							targets: {
								type: 'array',
								items: {
									type: 'string',
								},
							},
							opcode: {
								type: 'number',
							},
							encrypted: {
								type: 'boolean',
							},
							massive: {
								type: 'boolean',
							},
							data: {
								type: 'array',
								items: {
									type: 'number',
								},
							},
						},
					},
					{
						type: 'array',
						items: {
							type: 'string',
						},
					},
				],
			},
		},
		required: ['operation', 'data'],
	},
	response: {
		200: {
			type: 'object',
			properties: {
				status: {
					type: 'string',
				},
			},
		},
	},
};
