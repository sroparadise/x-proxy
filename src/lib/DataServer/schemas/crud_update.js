export default {
	params: {
		type: 'object',
		properties: {
			model: {type: 'string'},
		},
		required: ['model'],
	},
	body: {
		type: 'object',
		properties: {
			where: {
				type: 'object',
			},
			payload: {
				type: 'object',
			},
		},
		required: ['where', 'payload'],
	},
	response: {
		200: {
			type: 'object',
		},
	},
};
