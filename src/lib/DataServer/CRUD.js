import {buildFilter} from 'objection-filter';

class CRUD_Error extends Error {
	constructor(message = 'UNKNOWN_ERROR') {
		super();
		this.message = message;
		this.statusCode = 400;
	}
}

class CRUD {
	constructor(model) {
		this.model = model;
	}

	async create(data = {}) {
		try {
			if (data === {}) throw 'INVALID_DATA';

			return await this.model.query().insertGraph(data);
		} catch (e) {
			throw new CRUD_Error(e);
		}
	}

	async read(filter = {}, page = false, records = 50) {
		try {
			if (filter === {}) throw 'INVALID_FILTER';

			filter.eager = filter.query;
			delete filter.query;

			const query = buildFilter(this.model, null, {
				operators: {
					$notIn: (property, operand, builder) => builder.whereNotIn(property, operand),
				},
			}).build(filter);

			const data = page ? await query.page(page, records) : await query;

			return data.results
				? data
				: {
						results: data,
						total: data.length,
				  };
		} catch (e) {
			throw new CRUD_Error(e);
		}
	}

	async update(where = {}, payload = {}) {
		try {
			if (where === {}) throw 'INVALID_FILTER';
			if (payload === {}) throw 'INVALID_PAYLOAD';

			return await this.model.query().where(where).update(payload);
		} catch (e) {
			throw new CRUD_Error(e);
		}
	}

	async delete(where = {}) {
		try {
			if (where === {}) throw 'INVALID_FILTER';

			return await this.model.query().where(where).delete();
		} catch (e) {
			throw new CRUD_Error(e);
		}
	}
}

export default CRUD;
