import {Model} from 'objection';
class RefObjCommon extends Model {
	static get tableName() {
		return 'SILKROAD_R_SHARD.dbo._RefObjCommon';
	}

	static get idColumn() {
		return 'idx';
	}
}

export default RefObjCommon;
