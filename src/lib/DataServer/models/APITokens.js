import APITokensLink from './APITokensLink';
import Model from './BaseModel';
import User from './User';

class APITokens extends Model {
	static get tableName() {
		return 'SILKROAD_PROXY.dbo.api_tokens';
	}

	static get idColumn() {
		return 'id';
	}

	static get relationMappings() {
		return {
			user: {
				relation: Model.HasOneRelation,
				modelClass: User,
				join: {
					from: 'SILKROAD_PROXY.dbo.api_tokens.UserJID',
                    to: 'SILKROAD_R_ACCOUNT.dbo.TB_User.JID',
				},
			},
            link: {
                relation: Model.HasOneRelation,
                modelClass: APITokensLink,
                join: {
					from: 'SILKROAD_PROXY.dbo.api_tokens.id',
                    to: 'SILKROAD_PROXY.dbo.api_tokens_link.token_id',
				},
            }
		};
	}
}

export default APITokens;
