import Model from './BaseModel';
import Users from './User';
import Characters from './Character';

class Achievements extends Model {
	static get tableName() {
		return 'SILKROAD_PROXY.dbo.achievements';
	}

	static get idColumn() {
		return 'id';
	}

	static get jsonSchema() {
		return {
			type: 'object',
			required: ['UserJID', 'CharID', 'type', 'status', 'meta'],
			properties: {
				UserJID: {type: 'integer'},
				CharID: {type: 'integer'},
				type: {type: 'string'},
				status: {type: 'string', enum: ['pending', 'completed', 'cancelled']},
				meta: {type: 'object'},
			},
		};
	}

	static get relationMappings() {
		return {
			user: {
				relation: Model.BelongsToOneRelation,
				modelClass: Users,
				join: {
					from: 'SILKROAD_PROXY.dbo.chat_logs.UserJID',
					to: 'SILKROAD_R_ACCOUNT.dbo.TB_User.JID',
				},
			},
			character: {
				relation: Model.BelongsToOneRelation,
				modelClass: Characters,
				join: {
					from: 'SILKROAD_PROXY.dbo.chat_logs.CharID',
					to: 'SILKROAD_R_SHARD.dbo._Char.CharID',
				},
			},
		};
	}
}

export default Achievements;
