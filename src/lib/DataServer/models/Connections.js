import Model from './BaseModel';
import User from './User';
import Character from './Character';

class Connnections extends Model {
	static get tableName() {
		return 'SILKROAD_PROXY.dbo.connections';
	}

	static get idColumn() {
		return 'id';
	}

	static get relationMappings() {
		return {
			user: {
				relation: Model.BelongsToOneRelation,
				modelClass: User,
				join: {
					from: 'SILKROAD_PROXY.dbo.connections.UserJID',
					to: 'SILKROAD_R_ACCOUNT.dbo.TB_User.JID',
				},
			},
			character: {
				relation: Model.BelongsToOneRelation,
				modelClass: Character,
				join: {
					from: 'SILKROAD_PROXY.dbo.connections.CharID',
					to: 'SILKROAD_R_SHARD.dbo._Char.CharID',
				},
			},
		};
	}
}

export default Connnections;
