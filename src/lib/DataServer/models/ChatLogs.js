import Model from './BaseModel';
import User from './User';
import Character from './Character';

class ChatLogs extends Model {
	static get tableName() {
		return 'SILKROAD_PROXY.dbo.chat_logs';
	}

	static get idColumn() {
		return 'id';
	}

	static get jsonSchema() {
		return {
			type: 'object',
			required: ['UserJID', 'CharID', 'type', 'message'],
			properties: {
				UserJID: {type: 'integer'},
				CharID: {type: 'integer'},
				receiver: {type: ['null', 'string']},
				type: {type: 'integer'},
				message: {type: 'string'},
			},
		};
	}

	static get relationMappings() {
		return {
			user: {
				relation: Model.BelongsToOneRelation,
				modelClass: User,
				join: {
					from: 'SILKROAD_PROXY.dbo.chat_logs.UserJID',
					to: 'SILKROAD_R_ACCOUNT.dbo.TB_User.JID',
				},
			},
			character: {
				relation: Model.BelongsToOneRelation,
				modelClass: Character,
				join: {
					from: 'SILKROAD_PROXY.dbo.chat_logs.CharID',
					to: 'SILKROAD_R_SHARD.dbo._Char.CharID',
				},
			},
		};
	}
}

export default ChatLogs;
