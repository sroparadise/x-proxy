import {Model} from 'objection';
import UserCharacters from './UserCharacters';
import ChatMessages from './ChatLogs';

class Character extends Model {
	static get tableName() {
		return 'SILKROAD_R_SHARD.dbo._Char';
	}

	static get idColumn() {
		return 'CharID';
	}

	static get relationMappings() {
		return {
			characterLink: {
				relation: Model.BelongsToOneRelation,
				modelClass: UserCharacters,
				join: {
					from: 'SILKROAD_R_SHARD.dbo._Char.CharID',
					to: 'SILKROAD_R_SHARD.dbo._User.CharID',
				},
			},
			chatMessages: {
				relation: Model.HasManyRelation,
				modelClass: ChatMessages,
				join: {
					from: 'SILKROAD_R_SHARD.dbo._Char.CharID',
					to: 'SILKROAD_PROXY.dbo.chat_logs.CharID',
				},
			},
		};
	}
}

export default Character;