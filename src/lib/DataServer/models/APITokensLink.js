import Model from './BaseModel';
import User from './User';
import APITokens from "./APITokens";

class APITokensLink extends Model {
	static get tableName() {
		return 'SILKROAD_PROXY.dbo.api_tokens_link';
	}

	static get idColumn() {
		return 'id';
	}

	static get relationMappings() {
		return {
			token: {
				relation: Model.HasOneRelation,
				modelClass: APITokens,
				join: {
					from: 'SILKROAD_PROXY.dbo.api_tokens_link.token_id',
                    to: 'SILKROAD_PROXY.dbo.api_tokens.id',
				},
			},
		};
	}
}

export default APITokensLink;
