import {Model} from 'objection';

class BaseModel extends Model {
	constructor() {
		super();
	}

	$beforeInsert() {
		this.created = new Date();
	}

	$beforeUpdate() {
		this.updated = new Date();
	}
}

export default BaseModel;
