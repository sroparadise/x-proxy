import {Model} from 'objection';
import APITokens from './APITokens';
import Connections from './Connections';
import UserChannel from './UserChannel';
import UserCharacters from './UserCharacters';
import Wallet from './Wallet';
import WebItemCertifyKey from './WebItemCertifyKey';

class User extends Model {
	static get tableName() {
		return 'SILKROAD_R_ACCOUNT.dbo.TB_User';
	}

	static get idColumn() {
		return 'JID';
	}

	static get jsonSchema() {
		return {
			type: 'object',
			required: ['UserJID', 'CharID', 'type', 'message'],
			properties: {
				JID: {type: 'integer'},
				StrUserID: {type: 'integer'},
				StrEmail: {type: ['null', 'string']},
				sec_primary: {type: 'integer'},
				sec_content: {type: 'integer'},
			},
		};
	}

	static get relationMappings() {
		return {
			characterLink: {
				relation: Model.HasManyRelation,
				modelClass: UserCharacters,
				join: {
					from: 'SILKROAD_R_ACCOUNT.dbo.TB_User.JID',
					to: 'SILKROAD_R_SHARD.dbo._User.UserJID',
				},
			},
			wallet: {
				relation: Model.HasOneRelation,
				modelClass: Wallet,
				join: {
					from: 'SILKROAD_R_ACCOUNT.dbo.TB_User.JID',
					to: 'SILKROAD_R_ACCOUNT.dbo.SK_Silk.JID',
				},
			},
			connection: {
				relation: Model.BelongsToOneRelation,
				modelClass: Connections,
				join: {
					from: 'SILKROAD_R_ACCOUNT.dbo.TB_User.JID',
					to: 'SILKROAD_PROXY.dbo.connections.UserJID',
				},
			},
			mall_token: {
				relation: Model.HasManyRelation,
				modelClass: WebItemCertifyKey,
				join: {
					from: 'SILKROAD_R_ACCOUNT.dbo.TB_User.JID',
					to: 'SILKROAD_R_ACCOUNT.dbo.WEB_ITEM_CERTIFYKEY.UserJID',
				},
			},
			channel: {
				relation: Model.HasOneRelation,
				modelClass: UserChannel,
				join: {
					from: 'SILKROAD_R_ACCOUNT.dbo.TB_User.PortalJID',
					to: 'SILKROAD_R_ACCOUNT.dbo.TB_User_Channel.PortalJID',
				},
			},
			api_token: {
				relation: Model.HasOneRelation,
				modelClass: APITokens,
				join: {
					from: 'SILKROAD_R_ACCOUNT.dbo.TB_User.JID',
					to: 'SILKROAD_PROXY.dbo.api_tokens.UserJID',
				},
			},
		};
	}
}

export default User;
