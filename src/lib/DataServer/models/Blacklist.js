import Model from './BaseModel';

class Blacklist extends Model {
	static get tableName() {
		return 'SILKROAD_PROXY.dbo.blacklist';
	}

	static get idColumn() {
		return 'id';
	}
}

export default Blacklist;
