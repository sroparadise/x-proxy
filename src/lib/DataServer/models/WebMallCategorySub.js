import {Model} from 'objection';
import WebPackageItem from './WebPackageItem';

class WebMallCategorySub extends Model {
	// this table needs idx column
	static get tableName() {
		return 'SILKROAD_R_ACCOUNT.dbo.WEB_MALL_CATEGORY_SUB';
	}

	static get idColumn() {
		return 'sub_name_us';
	}

	static get relationMappings() {
		return {
			items: {
				relation: Model.HasManyRelation,
				modelClass: WebPackageItem,
				join: {
					from: 'SILKROAD_R_ACCOUNT.dbo.WEB_MALL_CATEGORY_SUB.sub_no',
					to: 'SILKROAD_R_ACCOUNT.dbo.WEB_PACKAGE_ITEM.shop_no_sub',
				},
			},
		};
	}
}

export default WebMallCategorySub;
