import {Model} from 'objection';
import WebPackageItem from './WebPackageItem';
import WebPackageItemDetail from './WebPackageItemDetail';
import WebPackageItemLang from './WebPackageItemLang';
import WebPackageItemPreview from './WebPackageItemPreview';

class WebPackageItemMall extends Model {
	// this table needs idx column
	static get tableName() {
		return 'SILKROAD_R_ACCOUNT.dbo.WEB_PACKAGE_ITEM_MALL';
	}

	static get idColumn() {
		return 'package_id';
	}

	static get relationMappings() {
		return {
			item: {
				relation: Model.HasOneRelation,
				modelClass: WebPackageItem,
				join: {
					from: 'SILKROAD_R_ACCOUNT.dbo.WEB_PACKAGE_ITEM_MALL.package_id',
					to: 'SILKROAD_R_ACCOUNT.dbo.WEB_PACKAGE_ITEM.package_id',
				},
			},
			detail: {
				relation: Model.HasOneRelation,
				modelClass: WebPackageItemDetail,
				join: {
					from: 'SILKROAD_R_ACCOUNT.dbo.WEB_PACKAGE_ITEM_MALL.package_id',
					to: 'SILKROAD_R_ACCOUNT.dbo.WEB_PACKAGE_ITEM_DETAIL.package_id',
				},
			},
			lang: {
				relation: Model.HasOneRelation,
				modelClass: WebPackageItemLang,
				join: {
					from: 'SILKROAD_R_ACCOUNT.dbo.WEB_PACKAGE_ITEM_MALL.package_id',
					to: 'SILKROAD_R_ACCOUNT.dbo.WEB_PACKAGE_ITEM_LANG.package_id',
				},
			},
			preview: {
				relation: Model.HasOneRelation,
				modelClass: WebPackageItemPreview,
				join: {
					from: 'SILKROAD_R_ACCOUNT.dbo.WEB_PACKAGE_ITEM_MALL.package_id',
					to: 'SILKROAD_R_ACCOUNT.dbo.WEB_PACKAGE_ITEM_PREVIEW.package_id',
				},
			},
		};
	}
}

export default WebPackageItemMall;
