import Model from './BaseModel';

class UserChannel extends Model {
	static get tableName() {
		return 'SILKROAD_R_ACCOUNT.dbo.TB_User_Channel';
	}

	static get idColumn() {
		return 'PortalJID';
	}
}

export default UserChannel;
