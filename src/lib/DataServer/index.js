import fastify from 'fastify';
import {Model} from 'objection';
import knex from 'knex';
import dbConfig from './config';
import models from './models';
import CRUD from './CRUD';
import {camelCase, startCase} from 'lodash';
import path from 'path';
import logger_instance from '@lib/utils/logger';
import * as schemas from './schemas';
import swagger from '@fastify/swagger';
import swaggerUi from '@fastify/swagger-ui';
import commConfig from '@config/PubSub';
import {encode} from '@lib/utils/pubsub/packet';
import publisher from '@lib/utils/pubsub/publisher';

const logger = logger_instance('DataServer', 'local');

class DataServer_Error extends Error {
	constructor(message = 'UNKNOWN_ERROR') {
		super();
		this.message = message;
		this.statusCode = 404;
	}
}

class DataServer {
	constructor(config) {
		this.config = config;
		this.publisher = publisher(commConfig.web);
	}

	async run() {
		const self = this;
		const server = fastify({...this.config, logger: false});
		const connection = knex(dbConfig);

		Model.knex(connection);

		const resolveModel = (model) => {
			try {
				const model_name = startCase(camelCase(model)).replace(/[^a-zA-Z]/g, '');
				return new CRUD(models[model_name]);
			} catch (e) {
				throw new DataServer_Error(e);
			}
		};

		await server.register(swagger, schemas.base_schema);
		await server.register(swaggerUi, {
			routePrefix: '/documentation',
			uiConfig: {
				docExpansion: 'list',
				deepLinking: false,
				displayOperationId: true,
				displayRequestDuration: true,
				syntaxHighlight: true,
				showExtensions: true,
				filter: true,
				layout: 'BaseLayout',
				syntaxHighlight: {
					activate: true,
					theme: 'nord',
				},
				persistAuthorization: true,
			},
			staticCSP: true,
			transformStaticCSP: (header) => header,
			...schemas.base_schema,
		});

		// PUBSUB
		server.route({
			method: 'POST',
			url: '/publish',
			schema: schemas.publish,
			async handler({body: {context, operation, data}}, reply) {
				self.publisher.send([
					context,
					encode({
						operation,
						data,
					}),
				]);

				return {status: 'OK'};
			},
		});

		// READ
		server.route({
			method: 'POST',
			url: '/:model/search',
			schema: schemas.crud_read,
			handler({query: {page, limit}, params: {model}, body}, reply) {
				const crud = resolveModel(model);
				return crud.read(body, page || false, limit || 100);
			},
		});

		// CREATE
		server.route({
			method: 'POST',
			url: '/:model',
			schema: schemas.crud_create,
			handler({params: {model}, body}, reply) {
				const crud = resolveModel(model);
				return crud.create(body);
			},
		});

		// UPDATE
		server.route({
			method: 'PUT',
			url: '/:model',
			schema: schemas.crud_update,
			handler({params: {model}, body: {where, payload}}, reply) {
				const crud = resolveModel(model);
				return crud.update(where, payload);
			},
		});

		// DELETE
		server.route({
			method: 'DELETE',
			url: '/:model',
			schema: schemas.crud_delete,
			handler({params: {model}, query}, reply) {
				const crud = resolveModel(model);
				return crud.delete(query);
			},
		});

		try {
			server.listen({
				host: this.config.host,
				port: this.config.port,
			});
			server.ready((err) => {
				if (err) throw new DataServer_Error(err.message);
				logger.log('info', 'listening', {
					host: this.config.port,
					port: this.config.host,
				});
				if (process.env.NODE_ENV !== 'production') {
					server.swagger();
					logger.log('info', 'documentation', {
						url: `http://127.0.0.1:${this.config.port}/documentation`,
					});
				}
			});
		} catch (err) {
			server.log.error(err);
			process.exit(1);
		}
	}
}

export default DataServer;
