import {encode} from '@lib/utils/pubsub/packet';
import {database} from '@lib/utils';

const controller = async ({publisher}) => {
	// Helper to send packets to a module connections:
	const msg = (data) => {
		// console.log(data);
		return publisher.send([
			'AgentServer',
			encode({
				operation: 'message',
				data,
			}),
		]);
	};


	// do stuff
};


export default {
	// When to run the task:
	schedules: [
		{
			minute: 59, // every 59th minute of an hour
		},
	],
	controller, // The controller script above
};
