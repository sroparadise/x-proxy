import scheduler from 'node-schedule';
import * as events from './events';
import publisher from '@lib/utils/pubsub/publisher';
import {logger as logger_instance} from '../utils';

const logger = logger_instance('EventsManager', 'local');
class EventsManager {
	constructor(config) {
		this.config = config;
		this.events = [];
	}

	async run() {
		this.publisher = publisher(this.config);

		for await (const event of Object.keys(events)) {
			const {schedules, controller} = events[event];

			schedules.map((schedule) => {
				this.events.push(
					scheduler.scheduleJob(schedule, async () => {
						logger.log('info', `task_start`, {event});
						try {
							await controller({publisher: this.publisher});
						} catch (error) {
							logger.log('error', `task_error`, {event, timestamp: new Date(), error});
						} finally {
							logger.log('info', `task_end`, {event, timestamp: new Date()});
						}
					})
				);

				logger.log('info', 'registered', {event, schedules});
			});
		}

		logger.log('info', 'ready', {...this.config, events: this.events.length});
	}
}

export default EventsManager;
