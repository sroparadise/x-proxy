export default [
  {
    question: `What's the starting Town for Chinese race characters?`,
    answer: "jangan",
  },
  {
    question: `Which is the most sized bird NPC in Silkroad world?`,
    answer: "roc",
  },
  {
    question: `Most common shorter form of saying "Forgotten World" is ...?`,
    answer: "fgw",
  },
  {
    question: `There's two... Devil Horse - but who is the other?`,
    answer: "demon horse",
  },
  {
    question: `Where are the Penon NPCs located at?`,
    answer: "karakoram",
  },
  {
    question: `What's the opposite color of Black?`,
    answer: "white",
  },
  +{
    question: `Odd one out: Apples, Carrots, Pears`,
    answer: "carrots",
  },
  {
    question: `Name the original creator of Silkroad Online...`,
    answer: "joymax",
  },
  {
    question: `Which race starts their game at Constantinopole town?`,
    answer: "european",
  },
  {
    question: `In what year were the first Air Jordan sneakers released?`,
    answer: "1984",
  },
  {
    question: `In a website browser address bar, what does "www" stand for?`,
    answer: `world wide web`,
  },
  {
    question: `Fissures, vents, and plugs are all associated with which geological feature?`,
    answer: `volcanos`,
    special: true,
  },
  {
    question: `What was the first toy to be advertised on television?`,
    answer: `potato head`,
  },
  {
    question: `What's the name of teleporter usually around center of each Town?`,
    answer: `dimensional gate`,
  },
  {
    question: `What's the name of storage keeper(female) in Jangan Town`,
    answer: `sansan`,
  },
  {
    question: `What's the official shiroi website address?`,
    answer: `shiroi.online`,
  },
  {
    question: `How many fortresses are there in Shiroi Online?`,
    answer: `four`,
  },
  {
    question: `Town that connects Western China and Asia is..?`,
    answer: `donwhang`,
  },
  {
    question: `What's the name of Donwhang Stable-Keeper?`,
    answer: `makgo`,
  },
  {
    question: `Who's the homeless guy in every Town?`,
    answer: `genie`,
  },
  {
    question: `The mighty ruler of Taklamakan is?`,
    answer: `lord yarkan`,
  },
  {
    question: `Name of the king that has the quest for last collection book?`,
    answer: `shahryar`,
  },
  {
    question: `Name the item required for yellow chat...`,
    answer: `global chatting`,
  },
  {
    question: `Which country consumes the most chocolate per capita?`,
    answer: `switzerland`,
  },
  {
    question: `What is the loudest animal on Earth?`,
    answer: `sperm whale`,
    special: true,
  },
  {
    question: `What was the name of the rock band formed by Jimmy Page?`,
    answer: `led zeppelin`,
  },
  {
    question: `What's the hardest rock?`,
    answer: `diamond`,
  },
  {
    question: `What color is Absinthe?`,
    answer: `green`,
  },
  {
    question: `The Statue of Liberty was given to the US by which country?`,
    answer: `france`,
  },
  {
    question: `Where is the Sea of Tranquility located?`,
    answer: `the moon`,
  },
  {
    question: `What country has the world's most ancient forest?`,
    answer: `australia`,
  },
  {
    question: `Odd one out: Internet, Gaming, Gardening`,
    answer: `gardening`,
  },
  {
    question: `Who invented scissors?`,
    answer: `leonardo da vinci`,
  },
  {
    question: `What animal is constitutionally protected in Florida?`,
    answer: `pigs`,
  },
  {
    question: `Who was said to "float like a butterfly and sting like a bee"?`,
    answer: `muhammed ali`,
  },
  {
    question: `Where did Heineken beer originate?`,
    answer: `the netherlands`,
  },
  {
    question: `Which country did bagels originate from?`,
    answer: `poland`,
  },
  {
    question: `What book starts with the line 'Call me Ishmael.'?`,
    answer: `moby dick`,
  },
  {
    question: `What is an ice hockey puck made from?`,
    answer: `rubber`,
  },
  {
    question: `Galileo was the citizen of which country?`,
    answer: `italy`,
  },
  {
    question: `What does a seismologist study?`,
    answer: `earthquakes`,
  },
  {
    question: `On average, what is the thing that Americans do 22 times in a day?`,
    answer: `open the fridge`,
  },
  {
    question: `Who composed the music for Sonic the Hedgehog 3?`,
    answer: `Michael Jackson`,
  },
  {
    question: `Who said, 'Champagne should be dry, cold, and free.'?`,
    answer: `winston churchill`,
  },
  {
    question: `In what country would one compete in a 'wife carry race'?`,
    answer: `Finland`,
  },
  {
    question: `What was Walt Disney afraid of?`,
    answer: `mice`,
  },
  {
    question: `Who is the sister-in-law of your dad's only brother?`,
    answer: `mother`,
  },
  {
    question: `What goes up and comes back down, but has still never moved?`,
    answer: `staircase`,
  },
  {
    question: `What is the fear of long words known as?`,
    answer: `hippopotomostrosesquippedaliophobia`,
    special: true,
  },
  {
    question: `What is a crossbreed between a zebra and a donkey called?`,
    answer: `a zonkey`,
  },
  {
    question: `What's the antonym of "synonym"?`,
    answer: `antonym`,
  },
  {
    question: `What do humans do 15 times a day on an average?`,
    answer: `laugh`,
  },
  {
    question: `What does bubble gum contain?`,
    answer: `rubber`,
  },
  {
    question: `What is lighter than a feather, but not possible to hold for even a few minutes?`,
    answer: `breath`,
  },
  {
    question: `What is a "Bombay Duck"?`,
    answer: `a fish`,
  },
  {
    question: `What's the ingredient that ancient romans mixed with boiled vinegar to achieve energy drink?`,
    answer: `goat poop`,
  },
  {
    question: `In Tennessee, It's illegal to drive if you are...?`,
    answer: `asleep`,
  },
  {
    question: `What's Johnny Depp afraid of?`,
    answer: `clowns`,
  },
  {
    question: `A group of ravens are known as?`,
    answer: `unkindness`,
  },
  {
    question: `Who invented the word "Vomit"?`,
    answer: `william shakespeare`,
  },
  {
    question: `Coprastastaphobia is the fear of...?`,
    answer: `constipation`,
  },
  {
    question: `Say "CHEESE"!`,
    answer: `cheese`,
    special: true,
  },
  {
    question: `What is the largest animal in the world?`,
    answer: `the blue whale`,
  },
  {
    question: `Who is Mickey Mouse's wife?`,
    answer: `minnie mouse`,
  },
  {
    question: `Jack and Jill were going up what?`,
    answer: `the hill`,
  },
  {
    question: `Name the fastest animal on earth..`,
    answer: `cheetah`,
  },
  {
    question: `Who is SpongeBob's best friend?`,
    answer: `patrick star`,
  },
  {
    question: `Who is Batman's sidekick?`,
    answer: `Robin`,
  },
  {
    question: `Which Disney princess lost her shoe?`,
    answer: `Cinderella`,
  },
  {
    question: `Who is the villain in Aladdin?`,
    answer: `Jafar`,
  },
  {
    question: `Who is Ash Ketchum's yellow Pokémon?`,
    answer: `Pikachu`,
  },
  {
    question: `Which travels faster, light or sound?`,
    answer: `light`,
  },
  {
    question: `Which planet is closest to the sun?`,
    answer: `Mercury`,
  },
  {
    question: `Where is the Leaning Tower of Piza?`,
    answer: `Italy`,
  },
  {
    question: `The Little Mermaid had a pet fish named what? `,
    answer: `Flounder`,
  },
  {
    question: `What language, other than English, do they speak in Canada? `,
    answer: `French`,
  },
  {
    question: `Cleopatra was the queen of which country? `,
    answer: `Egypt`,
  },
  {
    question: `What do astronomers study?`,
    answer: `The stars`,
  },
  {
    question: `What did Julius Caesar rule?`,
    answer: `Rome`,
  },
  {
    question: `Which bone is in your head?`,
    answer: `The skull`,
  },
  {
    question: `What do you call a baby cow?`,
    answer: `A calf`,
  },
  {
    question: `Which animal was considered sacred in Ancient Egypt?`,
    answer: `Cats`,
  },
  {
    question: `Who was the headmaster of Hogwarts School of Witchcraft and Wizardry?`,
    answer: `Albus Dumnbledore`,
  },
  {
    question: `What do you call a shape with 6 sides?`,
    answer: `A hexagon`,
  },
  {
    question: `How many Dragon Balls are there?`,
    answer: `Seven`,
  },
  {
    question: `Who solved the Millennium Puzzle?`,
    answer: `Yugi Mutou`,
    special: true,
  },
  {
    question: `Which anime series revolves around the Elric brothers?`,
    answer: `Fullmetal Alchemist`,
  },
  {
    question: `What's the name of Naruto's daughter?`,
    answer: `Himawari`,
  },
  {
    question: `The Shinigami is in love with apples. Can you recall his name?`,
    answer: `Ryuk`,
  },
  {
    question: `Who is known as the "One Punch Man"?`,
    answer: `Saitama`,
  },
  {
    question: `Who is a member of an extraterrestrial warrior race called the Saiyans?`,
    answer: `Goku`,
  },
  {
    question: `One for all and...`,
    answer: `All for one`,
  },
  {
    question: `What is Saitama's superhero name in the association?`,
    answer: `Bald Cape`,
  },
  {
    question: `Name of the highest-grossing anime and Japanese film of all time?`,
    answer: `Demon Slayer: Infinity Train`,
  },
  {
    question: `Finish the lyrics: "To catch them is my real test, to"...?`,
    answer: `train them is my cause`,
    special: true,
  },
  {
    question: `What's the opposite of brightness?`,
    answer: `Darkness`,
  },
];
