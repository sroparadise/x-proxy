import {ActionRowBuilder, ModalBuilder, TextInputBuilder, TextInputStyle} from 'discord.js';
import APITokensLink from '@lib/DataServer/models/APITokensLink';
import CRUD from '@lib/DataServer/CRUD';

async function linkAccountCommand(interaction) {
	const token_storage_link = new CRUD(APITokensLink);

	const {
		results: [data],
	} = await token_storage_link.read({
		query: {
			token: {
				user: {},
			},
			$where: {
				service_id: interaction.user.id,
			},
		},
		fields: ['token.user.StrUserID'],
	});

	if (data) {
		await interaction.reply({content: `\`\`\`md\r\n# Error\r\n- Your account is already linked!\r\n- Game Username: ${data.token.user.StrUserID}\`\`\``, ephemeral: true});
		return;
	}

	const modal = new ModalBuilder().setCustomId('linkCode').setTitle('Link Game Account');
	const tokenInput = new TextInputBuilder().setCustomId('gameToken').setLabel('Token').setStyle(TextInputStyle.Short).setMaxLength(32).setMinLength(32).setRequired(true);
	const firstActionRow = new ActionRowBuilder().addComponents(tokenInput);

	modal.addComponents(firstActionRow);

	await interaction.showModal(modal);
}

export default linkAccountCommand;
