import APITokens from '@lib/DataServer/models/APITokens';
import APITokensLink from '@lib/DataServer/models/APITokensLink';
import CRUD from '@lib/DataServer/CRUD';

async function linkCodeModal(interaction) {
	if (interaction.customId === 'linkCode' && interaction.isModalSubmit()) {
		const token_storage = new CRUD(APITokens);
		const token_storage_link = new CRUD(APITokensLink);
		const token = interaction.fields.getTextInputValue('gameToken');

		const {
			results: [data],
		} = await token_storage.read({
			query: {
				user: {},
				link: {},
				$where: {
					token,
				},
			},
			fields: ['user.StrUserID'],
		});

		if (!data?.link) {
			await token_storage_link.create({
				token_id: data.id,
				service_id: interaction.user.id,
			});

			await interaction.reply({content: `\`\`\`md\r\n# Success\r\n- Welcome back, ${data.user.StrUserID}!\`\`\``, ephemeral: true});
		}
	}
}

export default linkCodeModal;
