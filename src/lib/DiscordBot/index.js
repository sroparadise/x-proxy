import {REST, Routes, Client, Events, GatewayIntentBits} from 'discord.js';
import {version, name, repository} from '@root/package.json';
import {linkCodeModal, linkAccountCommand} from './handlers';
import {Model} from 'objection';
import dbConfig from '@lib/DataServer/config';
import logger_instance from '@lib/utils/logger';
import knex from 'knex';

const logger = logger_instance('DiscordBot', 'local');

// Message when server is initialised:
const launch_template = `\`\`\`md
# Service Online
- Version: ${version} 
- Server Timestamp: ${new Date()}
\`\`\``;

class DiscordBot {
	constructor(config) {
		Model.knex(knex(dbConfig));

		this.config = config;
		this.rest = new REST({version: '10'}).setToken(config.token);
		this.client = new Client({
			intents: [GatewayIntentBits.Guilds, GatewayIntentBits.GuildMessages],
		});
	}

	async run() {
		// Insert commands list into discord api:
		await this.rest.put(Routes.applicationCommands(this.config.id), {body: this.config.commands});

		// Authenticate discord bot:
		this.client.login(this.config.token);

		// Bot ready:
		this.client.on('ready', () => {
			this.channel = this.client.channels.cache.find((channel) => channel.name === 'testing');

			this.client.on(Events.InteractionCreate, async (interaction) => {
				// Chat commands:
				if (interaction.isChatInputCommand()) {
					switch (interaction.commandName) {
						case 'linkaccount':
							linkAccountCommand(interaction);
							break;
						default:
							break;
					}
				}

				// Modal handlers:
				if (interaction.isModalSubmit()) {
					switch (interaction.customId) {
						case 'linkCode':
							linkCodeModal(interaction);
							break;
						default:
							break;
					}
				}
			});

			logger.log('info', 'ready', {
				appId: this.config.id,
				channel: this.channel.name,
			});

			this.channel.send(launch_template);
		});

		return this;
	}
}

export default DiscordBot;
