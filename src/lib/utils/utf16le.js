const utf16le = (str) => {
	let out;
	var i;
	let len;
	let c;
	let char2;
	let char3;

	out = '';
	len = str.length;
	i = 0;
	while (i < len) {
		c = str.charCodeAt(i++);
		switch (c >> 4) {
			case 0:
			case 1:
			case 2:
			case 3:
			case 4:
			case 5:
			case 6:
			case 7:
				// 0xxxxxxx
				out += str.charAt(i - 1);
				break;
			case 12:
			case 13:
				// 110x xxxx   10xx xxxx
				char2 = str.charCodeAt(i++);
				out += String.fromCharCode(((c & 0x1f) << 6) | (char2 & 0x3f));
				out += str.charAt(i - 1);
				break;
			case 14:
				// 1110 xxxx  10xx xxxx  10xx xxxx
				char2 = str.charCodeAt(i++);
				char3 = str.charCodeAt(i++);
				out += String.fromCharCode(((c & 0x0f) << 12) | ((char2 & 0x3f) << 6) | ((char3 & 0x3f) << 0));
				break;
		}
	}

	const byteArray = new Uint8Array(out.length * 2);
	for (var i = 0; i < out.length; i++) {
		byteArray[i * 2] = out.charCodeAt(i); // & 0xff;
		byteArray[i * 2 + 1] = out.charCodeAt(i) >> 8; // & 0xff;
	}

	return String.fromCharCode(...byteArray);
};

export default utf16le;
