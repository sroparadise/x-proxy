import {encode, decode} from './gbk';
import utf16le from './utf16le';

class stream {
	constructor() {}
}

class writer extends stream {
	constructor() {
		super();
		this.buffer = Buffer.alloc(4096);
		this.pointer = 0;
		this.size = 0;
	}

	uint8(number = 0) {
		this.buffer.writeUInt8(number, this.pointer);

		if (this.pointer == this.size) {
			this.pointer++;
			this.size++;
		} else {
			this.pointer++;
		}
	}

	uint16(number = 0) {
		this.buffer.writeUInt16LE(number, this.pointer);

		if (this.pointer == this.size) {
			this.pointer += 2;
			this.size += 2;
		} else {
			this.pointer += 2;
		}
	}

	uint32(number = 0) {
		this.buffer.writeUInt32LE(number, this.pointer);

		if (this.pointer == this.size) {
			this.pointer += 4;
			this.size += 4;
		} else {
			this.pointer += 4;
		}
	}

	float(number = 0.0) {
		this.buffer.writeFloatLE(number, this.pointer);

		if (this.pointer == this.size) {
			this.pointer += 4;
			this.size += 4;
		} else {
			this.pointer += 4;
		}
	}

	text(string = '') {
		const converted = utf16le(string);
		const len = string.length;

		this.uint16(len);

		const data = [...this.buffer.toJSON().data.slice(0, this.pointer), ...converted.toJSON().data];

		this.buffer = Buffer.from(data);

		if (this.pointer === this.size) {
			this.pointer += len;
			this.size += len * 2;
		} else {
			this.pointer += len;
		}
	}

	string(string = '') {
		const data = encode(string);
		const len = data.length;

		this.uint16(len);

		this.buffer.set(data, this.pointer);

		if (this.pointer == this.size) {
			this.pointer += len;
			this.size += len;
		} else {
			this.pointer += len;
		}
	}

	toData() {
		return this.buffer.slice(0, this.size).toJSON().data;
	}

	fromSchema(schema = {}, data = {}) {
		const keys = Object.keys(schema);
		let last_value = null;

		for (const key of keys) {
			const is_many = typeof schema[key] === 'object';

			if (is_many) {
				for (const index of [...Array(last_value).keys()]) {
					for (const _key of Object.keys(data[key][index])) {
						this[schema[key][_key]](data[key][index][_key]);
					}
				}
			} else {
				last_value = data[key];
				this[schema[key]](data[key]);
			}
		}

		return this.toData();
	}
}

class reader extends stream {
	constructor(data = []) {
		super();
		this.buffer = Buffer.from(data);
		this.pointer = 0;
	}

	uint8() {
		const value = this.buffer.readUInt8(this.pointer);
		this.pointer++;
		return value;
	}

	uint16() {
		const value = this.buffer.readUInt16LE(this.pointer);
		this.pointer += 2;
		return value;
	}

	uint32() {
		const value = this.buffer.readUInt32LE(this.pointer);
		this.pointer += 4;
		return value;
	}

	uint64() {
		const value = this.buffer.readUIntLE(this.pointer, 8);
		this.pointer += 8;
		return value;
	}

	text() {
		const len = this.uint16() * 2;
		const data = this.buffer.slice(this.pointer, this.pointer + len);
		let string = decode(data.toJSON().data);

		string = string.replaceAll('\u0000', '');

		this.pointer += len;
		return string;
	}

	string(length = 0) {
		const len = length || this.uint16();
		const data = this.buffer.slice(this.pointer, this.pointer + len);
		const string = decode(data.toJSON().data);

		this.pointer += len;
		return string;
	}

	fileString(length = 0) {
		const len = this.uint32();
		const data = this.buffer.slice(this.pointer, this.pointer + len);
		const string = decode(data.toJSON().data);

		this.pointer += len;
		return string;
	}

	vector2() {
		return {
			x: this.float(),
			y: this.float(),
		};
	}

	vector3() {
		return {
			x: this.float(),
			y: this.float(),
			z: this.float(),
		};
	}

	vector4() {
		return {
			w: this.float(),
			x: this.float(),
			y: this.float(),
			z: this.float(),
		};
	}

	rawBuffer(length = 0) {
		const data = this.buffer.slice(this.pointer, this.pointer + length);

		this.pointer += length;

		return data;
	}

	float() {
		const value = this.buffer.readFloatLE(this.pointer);
		this.pointer += 4;
		return value;
	}

	bool() {
		return this.buffer.readUInt8(this.pointer) == 1;
	}

	toData() {
		return this.buffer.toJSON().data;
	}

	fromSchema(schema = {}) {
		const keys = Object.keys(schema);
		const result = {};

		this.pointer = 0;
		for (const key of keys) {
			const is_many = typeof schema[key] === 'object';

			if (is_many) {
				const last_key = Object.keys(result).pop();
				result[key] = [];
				if (result[last_key] > 0) {
					[...Array(last_key).keys()].forEach((index) => {
						result[key].push(
							Object.keys(schema[key]).reduce(
								(obj, _key) => ({
									...obj,
									[_key]: this[schema[key][_key]](),
								}),
								{}
							)
						);
					});
				}
			} else {
				result[key] = this[schema[key]]();
			}
		}

		return result;
	}
}

export {writer, reader, stream};
