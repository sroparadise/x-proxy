import axios from 'axios';
import dotenv from 'dotenv';

dotenv.config();

const {DATA_API_URL} = process.env;

export default axios.create({
	baseURL: DATA_API_URL || `http://127.0.0.1:6440`,
});
