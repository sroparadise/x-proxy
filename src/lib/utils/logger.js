import path from 'path';
import moment from 'moment';
import {createLogger, format, transports} from 'winston';

const formatLog = ({combine, timestamp, prettyPrint, colorize, splat, simple, errors, printf}) =>
	combine(
		colorize(),
		timestamp(),
		prettyPrint(),
		splat(),
		simple(),
		errors(),
		printf(({timestamp, level, message, ...data}) => {
			let json = JSON.stringify(data, null, 2);
			json = json === '{}' ? '' : json;
			return `[${moment(timestamp).format("YYYY-MM-DD/HH:mm:ss")}][PID:${process.pid}] ${level} - ${message} ${json}`;
		})
	);

const logger = (module, ip) => {
	const is_dev = process.env.NODE_ENV !== 'production'
	const init_timestamp = moment().format('MM-DD-YYYY-HH');
	const filename = path.join(process.cwd(), 'logs', `${module}`, `${init_timestamp}_${ip}${is_dev ? '-dev' : ''}.log`);
	
	const log_transport = {
		console: new transports.Console({
			format: formatLog(format),
		}),
		file: new transports.File({filename}),
	};

	return createLogger({
		transports: is_dev ? [log_transport.console, log_transport.file] : [log_transport.file],
	});
};

export default logger;
