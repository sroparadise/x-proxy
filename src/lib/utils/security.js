import {writer, reader} from './stream';
import SSA from '@security/Security.node';

class Security extends SSA.Security {
	constructor(identity_name = 'SR_Client', identity_flag = 0) {
		super(identity_name, identity_flag);
	}

	generateHandshake(blowfish = true, security_bytes = true, handshake = true) {
		return super.GenerateHandshake(blowfish, security_bytes, handshake);
	}

	changeIdentity(identity_name = String, identity_flag = Number) {
		return super.ChangeIdentity(identity_name, identity_flag);
	}

	processIncomingStream() {
		return super.GetPacketToRecv() || [];
	}

	processOutgoingStream() {
		return super.GetPacketToSend() || [];
	}

	receive(data = Buffer) {
		return super.Recv(data.toJSON().data);
	}

	send(opcode, data, encrypted, massive) {
		return super.Send(opcode, data, encrypted, massive);
	}
}

export const Writer = writer;
export const Reader = reader;
export default Security;
