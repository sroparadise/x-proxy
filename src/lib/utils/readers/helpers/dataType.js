const types = {
	0x00000000: 'Mtrl',
	0x00010000: 'TexAni',
	0x00010001: 'MultiTex',
	0x00010002: 'MultiTexRev',
	0x00030000: 'Particle',
	0x00040000: 'EnvMap',
	0x00040001: 'BumpEnv',
	0x00050000: 'Sound',
	0x00060000: 'DynamicVertex',
	0x00060001: 'DynamicJoint',
	0x00060002: 'DynamicLattice',
	0x00070000: 'ProgEquipPow',
};

const readers = {
	DynamicJoint: (read) => ({}),
	DynamicLattice: (read) => ({}),
	DynamicVertex: (read) => ({}),
	Mtrl: (read) => {
		const data = {
			length: read.uint32(),
			flag: read.uint32(),
			_u_int_0: read.uint32(),
			gradients: {
				count: read.uint32(),
				items: [],
			},
		};

		console.log({data})

		for (let i = 0; i < data.gradients.count; i++) {
			data.gradients.items.push({
				time: read.uint32(),
				color: {
					r: read.uint32(),
					g: read.uint32(),
					b: read.uint32(),
					a: read.uint32(),
				},
			});
		}

		data._u_int_1 = read.uint32();
		data._u_int_2 = read.uint32();
		data._u_int_3 = read.uint32();
		data._u_int_4 = read.uint32();
		data._u_int_5 = read.uint32();
		data._u_int_6 = read.uint32();
		data._u_int_7 = read.uint32();
		data._u_int_8 = read.uint32();
		data._u_int_9 = read.uint32();

		return data;
	},
	MultiTex: (read) => ({
		_u_int_0: read.uint32(),
		texture: read.fileString(),
		_u_int_1: read.uint32(),
	}),
	MultiTexRev: (read) => ({
		_u_int_0: read.uint32(),
		texture: read.fileString(),
		_u_int_1: read.uint32(),
	}),
	Particle: (read) => {
		const data = {
			count: read.uint32(),
			items: [],
		};

		for (let i = 0; i < data.count; i++) {
			const p_data = {
				enabled: read.uint32(),
				file: read.fileString(),
				boneFile: read.fileString(),
				position: {
					x: read.float(),
					y: read.float(),
					z: read.float(),
				},
				birthTime: read.uint32(),
				_u_byte_0: read.uint8(),
				_u_byte_1: read.uint8(),
				_u_byte_2: read.uint8(),
				_u_byte_3: read.uint8(),
			};

			if (p_data._u_byte_3 === 1) {
				p_data._u_vector3_0 = {
					x: read.float(),
					y: read.float(),
					z: read.float(),
				};
			}
		}

		return data;
	},
	ProgEquipPow: (read) => ({}),
	Sound: (read) => {
		const count = read.uint32();

		if (count > 0) {
			const data = {
				count,
				flag: read.uint32(),
				_u_int_0: read.uint32(),
				_u_int_1: read.uint32(),
				_u_float_0: read.float(),
				_u_float_1: read.float(),
				_u_int_2: read.uint32(),
				_u_int_3: read.uint32(),
				_u_int_4: read.uint32(),
				_u_int_5: read.uint32(),
				_u_int_6: read.uint32(),
				_u_int_7: read.uint32(),
				items: [],
			};

			for (let i = 0; i < data.count; i++) {
				const has_value = read.uint32();
				if (has_value === 1)
					data.items.push({
						file: read.fileString(),
						keyTime: read.uint32(),
						event: read.fileString(),
					});
			}

			return data;
		} else {
			return {};
		}
	},
	TexAni: (read) => ({
		_u_int_0: read.uint32(),
		_u_int_1: read.uint32(),
		_u_int_2: read.uint32(),
		_u_int_3: read.uint32(),
		_u_int_4: read.uint32(),
		_u_matrix: read.rawBuffer(64).toJSON().data,
	}),
	EnvMap: (read) => ({
		_u_int_0: read.uint32(),
		_u_int_1: read.uint32(),
		_u_int_2: read.uint32(),
		_u_int_3: read.uint32(),
	}),
	BumpEnv: (read) => {
		const data = {
			_u_int_0: read.uint32(),
			_u_int_1: read.uint32(),
			_u_int_2: read.uint32(),
			_u_int_3: read.uint32(),
			_u_int_4: read.uint32(),
			_u_int_5: read.uint32(),
			count: read.uint32(),
			items: [],
		};

		for (let i = 0; i < data.count; i++) {
			const has_value = read.uint8();
			if (has_value === 1) data.items.push(read.fileString());
		}
	},
};

const dataType = (type, read) => ({
    type_name: types[type],
	float0: read.float(),
	int0: read.uint32(),
	int1: read.uint32(),
	meshFileIdx: read.uint32(),
	int2: read.uint32(),
	int3: read.uint32(),
	byte0: read.uint8(),
	byte1: read.uint8(),
	byte2: read.uint8(),
	byte3: read.uint8(),
	...readers[types[type]](read),
});

export default dataType;
