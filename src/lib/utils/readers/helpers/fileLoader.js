import fs from 'node:fs';
import path from 'node:path';
import {reader} from '@util/stream';

const fileLoader = (filePath = String) => {
	const file_dir = path.join(process.cwd(), 'SR_DATA', filePath);
	const file = fs.readFileSync(file_dir);

	return new reader(file);
};

export default fileLoader;
