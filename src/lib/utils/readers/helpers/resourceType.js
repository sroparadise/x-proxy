const resourceType = {
	0x20000: 'Character',
	0x20001: 'NPC',
	0x20002: 'Building',
	0x20003: 'Artifact',
	0x20004: 'Nature',
	0x20005: 'Item',
	0x20006: 'Other',
	0x30000: 'CompoundCharacter',
	0x30002: 'CompoundObject',
};

export default resourceType;