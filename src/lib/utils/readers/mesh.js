import fileLoader from './helpers/fileLoader';

const mesh = (filePath = String) => {
	try {
		const read = fileLoader(filePath);

		const header = {
			signature: read.string(12),

			offsets: {
				vertex: read.uint32(),
				skin: read.uint32(),
				face: read.uint32(),
				cloth_vertex: read.uint32(),
				cloth_edge: read.uint32(),
				bounding_box: read.uint32(),
				occlusion: read.uint32(),
				nav_mesh: read.uint32(),
				skinned_nav_mesh: read.uint32(),
				_u_offset_1: read.uint32(), // find out
				_u_offset_2: read.uint32(), // find out
			},
			flags: {
				nav: read.uint32(),
				sub_prim: read.uint32(),
				vertex: read.uint32(),
				_u_flag_0: read.uint32(), // find out
			},
			name: read.fileString(),
			material: read.fileString(),
			_u_header_0: read.uint32(), // find out
		};
		
		const vertex = {
			count: read.uint32(),
			items: [],
		};

		function getVertexBuffer() {
			const vertexCount = read.uint32();
			const result = [];

			for (let i = 0; i < vertexCount; i++) {
				const vertex = {
					pos: {
						x: read.float(),
						y: read.float(),
						z: read.float(),
					},
					pos_normal: {
						x: read.float(),
						y: read.float(),
						z: read.float(),
					},
					uv0: {
						x: read.float(),
						y: read.float(),
					},
				};

				if (header.vertexFlag & 0x400) {
					vertex.uv1 = {
						x: read.float(),
						y: read.float(),
					};
				}

				if (header.vertexFlag & 0x800) {
					vertex.morphingData = read.rawBuffer(32);
				}

				// names of these?
				vertex.float0 = read.float();
				vertex.int0 = read.uint32();
				vertex.int1 = read.uint32();

				if (header.vertexFlag & 0x400) {
					vertex.lightmapPath = read.uint32();
				}

				result.push(vertex);
			}

			return {vertexCount, data: result};
		}

		function getSkinningData(vertexCount) {
			const boneCount = read.uint32();
			const boneNames = [];
			const result = [];

			if (boneCount > 0) {
				for (let i = 0; i < boneCount; i++) {
					const boneName = read.fileString();
					boneNames.push(boneName);
				}

				for (let i = 0; i < vertexCount; i++) {
					const boneStart = read.uint8();
					const boneStartWeight = read.uint16();
					const boneEnd = read.uint8();
					const boneEndWeight = read.uint16();

					result.push({
						boneStart,
						boneStartWeight,
						boneEnd,
						boneEndWeight,
					});
				}
			}

			return {
				boneCount,
				boneNames,
				data: result,
			};
		}

		function getIndexBuffer() {
			const faceCount = read.uint32();
			const result = [];

			for (let i = 0; i < faceCount; i++) {
				result.push({
					a: read.uint16(),
					b: read.uint16(),
					c: read.uint16(),
				});
			}

			return {
				faceCount,
				result,
			};
		}

		function getDyVertexData() {
			const clothVertexCount = read.uint32();
			const clothVertexData = [];
			const result = {};

			for (let i = 0; i < clothVertexCount; i++) {
				clothVertexData.push({
					maxDistance: read.float(),
					isPinned: read.uint32(),
				});
			}

			const clothEdgeCount = read.uint32();
			const clothEdges = [];
			const clothEdgeIndexes = [];

			if (clothEdgeCount > 0) {
				for (let i = 0; i < clothEdgeCount; i++) {
					clothEdges.push({
						a: read.uint32(),
						b: read.uint32(),
						maxDistance: read.float(),
					});
				}

				for (let i = 0; i < clothEdgeCount; i++) {
					clothEdgeIndexes.push(read.uint32());
				}

				result.deformationMode = read.uint32();
				result.animationOffsetX = read.float();
				result.animationOffsetZ = read.float();
				result.animationOffsetY = read.float();
				result.clothFallingSpeed = read.float();
				result._u_offset_1 = read.float();
				result._u_offset_2 = read.float();
				result.clothElasticity = read.float();
				result._u_offset_3 = read.uint32();
			}

			return {
				clothVertexCount,
				clothVertexData,
				clothEdges,
				clothEdgeIndexes,
				...result,
			};
		}

		function getBoundingBox() {
			return [...Array(6).keys()].map((_) => read.float());
		}

		function getOcclusionCullingData() {
			const hasOcclusionPortal = read.uint32();

			if (hasOcclusionPortal === 1) {
				const portalName = read.fileString();
				const vertexCount = read.uint32();
				const vertexes = [];
				const faces = [];

				for (let i = 0; i < vertexCount; i++) {
					vertexes.push({
						x: read.float(),
						y: read.float(),
						z: read.float(),
					});
				}

				const faceCount = read.uint32();

				for (let i = 0; i < faceCount; i++) {
					faces.push({
						a: read.uint16(),
						b: read.uint16(),
						c: read.uint16(),
					});
				}

				return {
					portalName,
					vertexCount,
					vertexes,
					faces,
				};
			}

			return false;
		}

		function getNavMeshObj(header) {
			if (header.navMeshOffset > 0) {
				const vertexCount = read.uint32();
				const vertexes = [];

				for (let i = 0; i < vertexCount; i++) {
					vertexes.push({
						pos: {
							x: read.float(),
							y: read.float(),
							z: read.float(),
						},
						bisectorIndex: read.uint8(),
					});
				}

				const collisionCellCount = read.uint32();
				const collisionCells = [];

				for (let i = 0; i < collisionCellCount; i++) {
					const collisionCell = {
						vertex0: read.uint16(),
						vertex1: read.uint16(),
						vertex2: read.uint16(),
						flag: read.uint16(),
					};

					if (header.navFlag & 2) {
						collisionCell.eventZoneData = read.uint8();
					}

					collisionCells.push(collisionCell);
				}

				const outlineLinkCount = read.uint32();
				const outlineEdges = [];

				for (let i = 0; i < outlineLinkCount; i++) {
					const edge = {
						srcVertex: read.uint16(),
						dstVertex: read.uint16(),
						srcCell: read.uint16(),
						dstCell: read.uint16(),
						flag: read.uint8(),
					};

					if (header.navFlag & 1) {
						edge.eventZoneData = read.uint8();
					}

					outlineEdges.push(edge);
				}

				const inlineLinkCount = read.uint32();
				const inlineEdges = [];

				for (let i = 0; i < inlineLinkCount; i++) {
					const edge = {
						srcVertex: read.uint16(),
						dstVertex: read.uint16(),
						srcCell: read.uint16(),
						dstCell: read.uint16(),
						flag: read.uint8(),
					};

					if (header.navFlag & 1) {
						edge.eventZoneData = read.uint8();
					}

					inlineEdges.push(edge);
				}

				let eventCount = 0;
				const events = [];

				if (header.navFlag & 4) {
					eventCount = read.uint32();
					for (let i = 0; i < eventCount; i++) {
						events.push(read.fileString());
					}
				}

				const grid = {
					origin: {
						x: read.float(),
						y: read.float(),
					},
					width: read.uint32(),
					height: read.uint32(),
					cellCount: read.uint32(),
					cells: [],
				};

				for (let i = 0; i < grid.cellCount; i++) {
					const gridCellOutlineCount = read.uint32();
					const gridCellOutlineIndexes = [];

					for (let _i = 0; _i < gridCellOutlineCount; _i++) {
						gridCellOutlineIndexes.push(read.uint16());
					}

					grid.cells.push({
						gridCellOutlineCount,
						gridCellOutlineIndexes,
					});
				}

				return {
					vertexCount,
					vertexes,
					collisionCellCount,
					collisionCells,
					outlineLinkCount,
					outlineEdges,
					inlineLinkCount,
					inlineEdges,
					eventCount,
					events,
					grid,
				};
			}

			return false;
		}

		function getSkinnedNavmesh() {
			try {
				const skinnedCount0 = read.uint32();

				const skinnedItems0 = [];
				for (let i = 0; i < skinnedCount0; i++) {
					skinnedItems0.push({
						x: read.float(),
						y: read.float(),
						z: read.float(),
					});
				}

				const skinnedCount1 = read.uint32();
				const skinnedItems1 = [];
				for (let i = 0; i < skinnedCount1; i++) {
					skinnedItems1.push({
						bone0: read.uint8(),
						bone1: read.uint8(),
					});
				}

				const skinnedCount2 = read.uint32();
				const skinnedItems2 = [];

				for (let i = 0; i < skinnedCount2; i++) {
					skinnedItems2.push({
						a: read.uint16(),
						b: read.uint16(),
						c: read.uint16(),
					});
				}

				return {
					skinnedCount0,
					skinnedItems0,
					skinnedCount1,
					skinnedItems1,
					skinnedCount2,
					skinnedItems2,
				};
			} catch (e) {
				// if no data, end of file reached
				return false;
			}
		}

		// const vertexBuffer = getVertexBuffer();
		// const skinningData = getSkinningData(vertexBuffer.vertexCount);
		// const indexBuffer = getIndexBuffer();
		// const dyVertexData = getDyVertexData();
		// const boundingBox = getBoundingBox();
		// const occlusionCullingData = getOcclusionCullingData();

		// // no variable here:
		// read.uint32();

		// const navMeshObj = getNavMeshObj(header);
		// const skinnedNavMesh = getSkinnedNavmesh();

		return {
			header,
			vertex,
			// // vertexBuffer,
			// // skinningData,
			// indexBuffer,
			// dyVertexData,
			// boundingBox,
			// occlusionCullingData,
			// navMeshObj,
			// skinnedNavMesh,
		};
	} catch (e) {
		console.log(e);
	}
};

export default mesh;
