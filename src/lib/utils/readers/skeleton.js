import fileLoader from './helpers/fileLoader';

const skeleton = (filePath = String) => {
	const read = fileLoader(filePath);

	const signature = read.string(12);

	const sub_prim = {
		count: read.uint32(),
		items: [],
	};

	for (let i = 0; i < sub_prim.count; i++) {
		const bone = {
			type: read.uint8(),
			name: read.fileString(),
			parent: read.fileString(),
			rotation: {},
			translation: {},
		};

		bone.rotation.parent = {
			w: read.float(),
			x: read.float(),
			y: read.float(),
			z: read.float(),
		};

		bone.translation.parent = {
			x: read.float(),
			y: read.float(),
			z: read.float(),
		};

		bone.rotation.origin = {
			w: read.float(),
			x: read.float(),
			y: read.float(),
			z: read.float(),
		};

		bone.translation.origin = {
			x: read.float(),
			y: read.float(),
			z: read.float(),
		};

		bone.rotation.local = {
			w: read.float(),
			x: read.float(),
			y: read.float(),
			z: read.float(),
		};

		bone.translation.local = {
			x: read.float(),
			y: read.float(),
			z: read.float(),
		};

		bone.children = {
			count: read.uint32(),
			items: [],
		};

		for (let _i = 0; _i < bone.children.count; _i++) {
			bone.children.items.push(read.fileString());
		}

		sub_prim.items.push(bone);
	}

	const _u_offset_1 = read.uint32();
	const _u_offset_2 = read.uint32();

	return {
		signature,
	    ...sub_prim,
		_u_offset_1,
		_u_offset_2,
	};
};

export default skeleton;
