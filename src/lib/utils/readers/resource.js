import fileLoader from './helpers/fileLoader';
import dataType from './helpers/dataType';
import resourceType from './helpers/resourceType';

const resource = (filePath = String) => {
	try {
		const read = fileLoader(filePath);

		const header = {
			signature: read.string(12),

			offsets: {
				material: read.uint32(),
				mesh: read.uint32(),
				skeleton: read.uint32(),
				animation: read.uint32(),
				mesh_group: read.uint32(),
				animation_group: read.uint32(),
				sound_effect: read.uint32(),
				bounding_box: read.uint32(),
			},

			flags: {
				mesh: read.uint32(),
				mod_data: read.uint32(),
				int2: read.uint32(),
				int3: read.uint32(),
				int4: read.uint32(),
			},
		};

		const info = {
			type: read.uint32(),
			name: read.fileString(),
		};

		const reserved = read.rawBuffer(48).toJSON().data;

		const bounding_box = {
			mesh: read.fileString().replace(/\\/g, '/'),
			box0: [...Array(6)].map((_) => read.float()),
			box1: [...Array(6)].map((_) => read.float()),
			has_additional_data: read.uint32(),
		};

		if (bounding_box.has_additional_data) bounding_box.additional_data = read.rawBuffer(64).toJSON().data;

		const materials = {
			count: read.uint32(),
			items: [],
		};

		for (let i = 0; i < materials.count; i++) {
			materials.items.push({
				id: read.uint32(),
				path: read.fileString().replace(/\\/g, '/'),
			});
		}

		const meshes = {
			count: read.uint32(),
			items: [],
		};

		for (let i = 0; i < meshes.count; i++) {
			const mesh = {
				path: read.fileString().replace(/\\/g, '/'),
			};

			if (header.primMeshFlag & 1) {
				mesh._u_int_0 = read.uint32();
			}
		}

		const animations = {
			typeVersion: read.uint32(),
			typeUserDefine: read.uint32(),
			count: read.uint32(),
			items: [],
		};

		for (let i = 0; i < animations.count; i++) {
			animations.items.push(read.fileString());
		}

		const skeleton = {
			exists: read.uint32(),
		};

		if (skeleton.exists === 1) {
			skeleton.path = read.fileString();
			skeleton.bone_path = read.fileString();
		}

		const mesh_groups = {
			count: read.uint32(),
			items: [],
		};

		for (let i = 0; i < mesh_groups.count; i++) {
			const mesh_group = {
				name: read.fileString(),
				count: read.uint32(),
				items: [],
			};

			for (let _i = 0; _i < mesh_group.count; _i++) {
				mesh_group.items.push(read.uint32());
			}

			mesh_groups.items.push(mesh_group);
		}

		const animation_groups = {
			count: read.uint32(),
			items: [],
		};

		for (let i = 0; i < animation_groups.count; i++) {
			const animation_group = {
				name: read.fileString(),
				count: read.uint32(),
			};

			for (let _i = 0; _i < animation_group.count; _i++) {
				const animation = {
					type: read.uint32(),
					fileIndex: read.uint32(),
					count: read.uint32(),
					items: [],
				};

				for (let __i = 0; __i < animation.count; __i++) {
					animation.items.push({
						keyTime: read.uint32(),
						type: read.uint32(),
						index: read.uint32(),
						_u_int_0: read.uint32(),
					});
				}

				const walk_points = {
					count: read.uint32(),
					walkLength: read.float(),
					items: [],
				};

				for (let __i = 0; __i < walk_points.count; __i++) {
					walk_points.items.push({
						x: read.float(),
						y: read.float(),
					});
				}
			}

			animation_groups.items.push(animation_group);
		}

		const system_mods = {
		    count: read.uint32(),
		    items: [],
		};

		for (let i = 0; i < system_mods.count; i++) {
		    const mod_set = {
		        type: read.uint32(),
		        animationType: read.uint32(),
		        name: read.fileString(),
		        count: read.uint32(),
		        items: [],
		    };

		    for (let _i = 0; _i < mod_set.count; _i++) {
		        const mod_type = read.uint32();
		        const mod_data = dataType(mod_type, read);

		        mod_set.items.push({
		            type: mod_type,
		            ...mod_data,
		        });
		    }

		    system_mods.items.push(mod_set);
		}

		const left_over_bytes = read.rawBuffer(read.buffer.length);

		return {
			header,
			info,
			bounding_box,
			reserved,
			materials,
			meshes,
			animations,
			skeleton,
			mesh_groups,
			animation_groups,
			system_mods,
			left_over_bytes,
		};
	} catch (e) {
		console.log(e);
	}
};

export default resource;
