import fileLoader from "./helpers/fileLoader";

const animation = (filePath = String) => {
	const read = fileLoader(filePath);

	const signature = read.string(12);

	function getHeader() {
		const data = {
			int0: read.uint32(),
			int1: read.uint32(),
			name_length: read.uint32(),
		};

		data.name = read.string(data.name_length);
		data.duration = read.uint32();
		data.fps = read.uint32();
		data.type = read.uint32();

		return data;
	}

	function getKeyFrames() {
		const keyFramesTimeCount = read.uint32();
		const keyFrameTimes = [];

		for (let i = 0; i < keyFramesTimeCount; i++) {
			const time = read.uint32();

			keyFrameTimes.push(time);
		}

		const animatedBoneCount = read.uint32();
		const animatedBones = [];

		for (let i = 0; i < animatedBoneCount; i++) {
			const boneName = read.fileString();
			const keyFrameCount = read.uint32();
			const keyFrames = [];

			for (let _i = 0; _i < keyFrameCount; _i++) {
				keyFrames.push({
					rotation: {
						w: read.float(),
						x: read.float(),
						y: read.float(),
						z: read.float(),
					},
					translation: {
						x: read.float(),
						y: read.float(),
						z: read.float(),
					},
				});
			}

			animatedBones.push({
				boneName,
				keyFrameCount,
				keyFrames,
			});
		}

		return {
			keyFramesTimeCount,
			keyFrameTimes,
			animationCount: animatedBoneCount,
			animation: animatedBones,
		};
	}
    
	return {
		signature,
		header: getHeader(),
		keyframes: getKeyFrames(),
	};
};

export default animation;
