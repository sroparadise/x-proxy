import fileLoader from './helpers/fileLoader';

const dungeon = (filePath = String) => {
	const read = fileLoader(filePath);

	const header = {
		signature: read.string(12),
		offsets: {
			block: read.uint32(),
			link: read.uint32(),
			grid: read.uint32(),
			group: read.uint32(),
			label: read.uint32(),
			_u_int_0: read.uint32(),
			_u_int_1: read.uint32(),
			bounding_box: read.uint32(),
		},
	};

	const info = {
		type: read.uint32(),
		name: read.fileString(),
		_u_offset_1: read.uint32(),
		_u_offset_2: read.uint32(),
		region_id: read.uint16(),
		collision_box_0: [...Array(6)].map((_) => read.float()),
		collision_box_1: [...Array(6)].map((_) => read.float()),
	};

	const blocks = {
		count: read.uint32(),
		items: [],
	};

	for (let i = 0; i < blocks.count; i++) {
		const block = {};

		block.path = read.fileString().replace(/\\/g, "/");
		block.name = read.fileString();

		block._u_offset_1 = read.uint32();

		block.position = {
			x: read.float(),
			y: read.float(),
			z: read.float(),
		};

		block.yaw = read.float();
		block.is_entrance = read.uint32();
		block.collision_box_0 = [...Array(6).keys()].map((_) => read.float());
		block._u_offset_2 = read.uint32();

		block.fog = {
			color: read.uint32(),
			near_plane: read.float(),
			far_plane: read.float(),
			intensity: read.float(),
			height_fog: read.uint8(),
		};

		if (block.fog.height_fog === 1) {
			block.fog.heightFog = {
				param1: read.float(),
				param2: read.float(),
				param3: read.float(),
				param4: read.float(),
			};
		}

		block._u_byte_1 = read.uint8();

		if (block._u_byte_1 === 0x02) {
			block._u_vector0 = {
				x: read.float(),
				y: read.float(),
				z: read.float(),
			};
			block._u_vector1 = {
				x: read.float(),
				y: read.float(),
				z: read.float(),
			};
			block._u_int = read.uint32();
		}

		block._u_string_0 = read.fileString();
		block.room_index = read.uint32();
		block.floor_index = read.uint32();

		block.indices = {
			connected: {},
			visible: {},
		};

		block.indices.connected.count = read.uint32();
		block.indices.connected.items = [...Array(block.indices.connected.count).keys()].map((_) => read.uint32());

		block.indices.visible.count = read.uint32();
		block.indices.visible.items = [...Array(block.indices.visible.count).keys()].map((_) => read.uint32());

		block.count = read.uint32();
		block.col_obj_count = read.uint32();

		block.items = [];

		for (let _i = 0; _i < block.count; _i++) {
			const obj = {
				name: read.fileString(),
				path: read.fileString().replace(/\\/g, "/"),
				position: {
					x: read.float(),
					y: read.float(),
					z: read.float(),
				},
				rotation: {
					x: read.float(),
					y: read.float(),
					z: read.float(),
				},
				scale: {
					x: read.float(),
					y: read.float(),
					z: read.float(),
				},
				flag: read.uint32(),
				_u_int_0: read.uint32(),
				radius_sqrt: read.float(),
			};

			if (obj.flag & 4) {
				obj.water_color = read.uint32();
			}

			block.items.push(obj);
		}

		block.lights = {
			count: read.uint32(),
			items: [],
		};

		for (let _i = 0; _i < block.lights.count; _i++) {
			block.lights.items.push({
				name: read.fileString(),
				position: {
					x: read.float(),
					y: read.float(),
					z: read.float(),
				},
				diffusion: {
					r: read.uint32(),
					g: read.uint32(),
					b: read.uint32(),
				},
				ambience: {
					r: read.uint32(),
					g: read.uint32(),
					b: read.uint32(),
				},
				specular: {
					r: read.uint32(),
					g: read.uint32(),
					b: read.uint32(),
				},
				transparency: {
					diffusion: read.float(),
					ambience: read.float(),
					specular: read.float(),
				},
			});
		}

		blocks.items.push(block);
	}

	const block_lookup_grid = {
		width: read.uint32(),
		height: read.uint32(),
		length: read.uint32(),
		buckets: {
			count: read.uint32(),
			items: [],
		},
	};

	for (let i = 0; i < block_lookup_grid.buckets.count; i++) {
		const id = read.uint32();
		const count = read.uint32();

		const items = [...Array(count).keys()].map((_) => read.uint32());

		block_lookup_grid.buckets.items.push({
			id,
			count,
			items,
		});
	}

	const links = {
		count: read.uint32(),
		items: [],
	};

	for (let i = 0; i < links.count; i++) {
		const count = read.uint32();

		const items = [...Array(count).keys()].map((_) => read.uint32());

		links.items.push({
			count,
			items,
		});
	}

	const labels = {
		room: {},
		floor: {},
	};

	if (header.offsets.label !== 0) {
		labels.room.count = read.uint32();
		labels.room.items = [];

		for (let i = 0; i < labels.room.count; i++) {
			labels.room.items.push(read.fileString());
		}

		labels.floor.count = read.uint32();
		labels.floor.items = [];

		for (let i = 0; i < labels.floor.count; i++) {
			labels.floor.items.push(read.fileString());
		}
	}

	const groups = {
		count: read.uint32(),
		items: [],
	};

	for (let i = 0; i < groups.count; i++) {
		const name = read.fileString();
		const service = read.uint32();
		const indices_count = read.uint32();
		const indices = [...Array(indices_count).keys()].map((_) => read.uint32());

		groups.items.push({
			name,
			service,
			indices_count,
			indices,
		});
	}

	return {
		header,
		info,
		blocks,
		block_lookup_grid,
		links,
		labels,
		groups,
	};
};

export default dungeon;
