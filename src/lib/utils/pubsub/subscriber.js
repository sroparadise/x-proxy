import zeromq from 'zeromq';
import logger_instance from '@lib/utils/logger';

function subscriber(config, module) {
	const logger = logger_instance('EventsManager', 'local');
	const socket = zeromq.socket('sub');

	socket.connect(`tcp://${config.host}:${config.port}`);

	socket.subscribe(module);

	logger.log('info', 'EventsManager:subscribe', {
		config,
		module,
	});

	return socket;
}

export default subscriber;
