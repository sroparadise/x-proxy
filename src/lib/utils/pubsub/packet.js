import json from 'fast-json-stringify';
import publish from '@lib/DataServer/schemas/publish';

export const fromSchema = json({
	title: 'packet',
	...publish.body,
});

export const encode = (data) => fromSchema(data);
export const decode = (data) => JSON.parse(data.toString());
