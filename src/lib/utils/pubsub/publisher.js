import zeromq from 'zeromq';
import logger_instance from '@lib/utils/logger';

function publisher(config) {
	const logger = logger_instance('EventsManager', 'local');
	const socket = zeromq.socket('pub');

	socket.bindSync(`tcp://${config.host}:${config.port}`);

	logger.log('info', 'EventsManager:publish', {
		config,
	});

	return socket;
}

export default publisher;
