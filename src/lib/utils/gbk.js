import data_table from './data/GBK';
/**
 *
 * @param {Array} buffer
 * @returns GBK/GB2312 decoded string
 */
const decode = (buffer = Array) => {
	let string = '';

	for (let n = 0, max = buffer.length; n < max; n++) {
		let code = buffer[n] & 0xff;
		if (code > 0x80 && n + 1 < max) {
			const code1 = buffer[n + 1] & 0xff;
			if (code1 >= 0x40) {
				code = data_table[((code << 8) | code1) - 0x8140];
				n++;
			}
		}
		string += String.fromCharCode(code);
	}

	return string;
};

/**
 *
 * @param {String} string
 * @returns GBK/GB2312 encoded Buffer
 */
const encode = (string = String) => {
	const result = [];
	const wh = '?'.charCodeAt(0);

	string += '';

	for (let i = 0; i < string.length; i++) {
		const charcode = string.charCodeAt(i);
		if (charcode < 0x80) result.push(charcode);
		else {
			let code = data_table.indexOf(charcode);
			if (~code) {
				code += 0x8140;
				result.push(0xff & (code >> 8), 0xff & code);
			} else {
				result.push(wh);
			}
		}
	}

	return result;
};

export {encode, decode};
