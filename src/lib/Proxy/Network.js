import net from 'net';
import path from 'path';
import EventEmitter from 'events';
import Session from '@lib/Proxy/Session';
import {logger, opcode, database} from '@lib/utils';
import {merge} from 'lodash';
import {FastRateLimit} from 'fast-ratelimit';
import {decode} from '@lib/utils/pubsub/packet';
import commConfig from '@config/PubSub';
import subscribe from '@lib/utils/pubsub/subscriber';

class Network extends EventEmitter {
	constructor(config) {
		super();

		this.not_whitelisted_opcodes = [];
		this.is_development = process.env.NODE_ENV === 'development';
		this.config = config;
		this.instances = {};

		this.limitter = new FastRateLimit({
			threshold: config.limits.PACKET_RATE_TRESHOLD,
			ttl: config.limits.PACKET_RATE_TTL,
		});

		this.connection_limitter = new FastRateLimit({
			threshold: config.limits.MAX_SIMULTANEOUS_IP_CONNECTIONS,
			ttl: 2,
		});

		this.logger = logger(`${this.config.module}_core`, 'network');

		// Add listeners for pub/sub messages
		this.attachCommunicationLayers();
	}

	attachCommunicationLayers() {
		this.subscriptions = {
			module: subscribe(commConfig.module, this.config.module),
			web: subscribe(commConfig.web, this.config.module_alias),
		};

		for (const listener of Object.keys(this.subscriptions)) {
			this.subscriptions[listener].on('message', (context, data) => {
				const decoded = decode(data);

				switch (decoded.operation) {
					case 'disconnect':
						this.disconnect(decoded.data);
						this.logger.log('info', 'broadcast:dc', {targets: decoded.data});
						break;
					case 'message':
						const {direction, targets, ...packet} = decoded.data;
						this.logger.log('info', 'broadcast:msg', {direction, targets, packet});
						this.injectBuffer(direction, targets, packet);
						break;
				}
			});
		}
	}

	async process(id, sender, buffer) {
		const target = sender === 'client' ? 'remote' : 'client';
		try {
			if (this.instances[id]) {
				let incoming_process = [];
				let outgoing_queue = [];
				let middlewares = [];

				this.instances[id].security[sender].receive(buffer);

				const incoming_queue = this.instances[id].security[sender].processIncomingStream();

				if (process.env?.LOG_PACKETS && sender === 'client') {
					this.instances[id].logger.log('info', 'stream', {
						incoming_queue: incoming_queue.map(i => ({
							...i,
							operation: opcode(i.opcode),
						})),
					});
				}

				// This block is dead dumb and will be removed in future, you can use this for understanding which opcodes are not whitelisted
				if (process.env?.SKIP_WHITELIST && sender === "client") {
					const not_whitelisted = incoming_queue.filter(
						(packet) =>
							target === 'remote' && !this.config.whitelist.includes(packet.opcode) && ![8194, 8193].includes(packet.opcode)
					);

					if (not_whitelisted.length) {
						this.logger.log('warn', 'not_whitelisted', {
							sender,
							target,
							packets: not_whitelisted.map((i) => ({
								...i,
								operation: opcode(i.opcode),
							})),
						});

						for (const i of not_whitelisted) {
							const to_whitelist = opcode(i.opcode);
							if (!this.not_whitelisted_opcodes.includes(to_whitelist)) this.not_whitelisted_opcodes.push(to_whitelist);
						}
					}

					incoming_process =
						target === 'client'
							? incoming_queue
							: incoming_queue.filter(
									(packet) =>
										target === 'remote' &&
										[...this.config.whitelist, ...not_whitelisted.map((p) => p.opcode)].includes(packet.opcode) &&
										packet.data.length <= this.config.limits.MAX_INCOMING_BUFFER_SIZE
							  );
				} 
				else {
					incoming_process =
						target === 'client'
							? incoming_queue
							: incoming_queue.filter(
									(packet) =>
										packet.data.length <= this.config.limits.MAX_INCOMING_BUFFER_SIZE &&
										target === 'remote' &&
										this.config.whitelist.includes(packet.opcode)
							  );
				}

				middlewares = incoming_process.reduce(
					(arr, packet) =>
						this.config.controllers[sender][packet.opcode]
							? [
									...arr,
									this.config.controllers[sender][packet.opcode](
										{...packet, direction: target},
										this.instances[id],
										this.config
									),
							  ]
							: arr,
					[]
				);

				middlewares = (await Promise.all(middlewares)) || [];
				middlewares = middlewares.flat(1);

				incoming_process = merge(incoming_process, middlewares);

				for (const {opcode, data, encrypted, massive} of incoming_process) {
					this.instances[id].security[target].send(opcode, data, encrypted, massive);
				}

				outgoing_queue = this.instances[id].security[target].processOutgoingStream();

				for (const data of outgoing_queue) {
					this.instances[id].connections[target].write(Buffer.from(data));
				}
			}
		} catch (e) {
			this.instances[id].logger.log('error', 'process', {
				sender,
				target,
				error: e?.message || 'UNKNOWN',
				stack: e?.stack || 'NONE',
			});
		}
	}

	async validateClient(remoteAddress) {
		try {
			const {
				data: {total: blacklist_entries},
			} = await database({
				method: 'POST',
				url: '/blacklist/search',
				data: {
					query: {
						$where: {
							remote: remoteAddress,
						},
					},
				},
			});

			const connection_count = Object.keys(this.instances).reduce(
				(count, id) => (this.instances[id].ip === remoteAddress ? count + 1 : count),
				0
			);

			return (
				blacklist_entries === 0 &&
				connection_count <= this.config.limits.MAX_SIMULTANEOUS_IP_CONNECTIONS &&
				this.connection_limitter.consumeSync(remoteAddress)
			);
		} catch (e) {
			return false;
		}
	}

	// process
	async registerClient(id, client) {
		const validClient = await this.validateClient(client.remoteAddress);
		if (validClient) {
			await database({
				method: 'POST',
				url: '/connections',
				data: {
					connection_id: id,
					context: this.config.module_alias,
					remote: client.remoteAddress,
					port: client.remotePort,
				},
			});

			this.instances[id] = new Session(id, client.remoteAddress, client.remotePort);

			this.instances[id].logger = logger(this.config.module, client.remoteAddress);

			this.instances[id].connections.client = client;

			this.connect(id);

			this.instances[id].logger.log('info', 'registerClient', {
				id,
			});

			this.instances[id].connections.client.on('data', (buffer) => this.rateLimit(id, buffer));

			this.instances[id].connections.client.on('error', (error) =>
				error?.code === 'ECONNRESET'
					? () => {}
					: this.logger.log('error', 'client_error', {
							id,
							error,
					  })
			);

			this.instances[id].connections.client.on('close', () => this.unregisterClient(id, 'client'));
		} else {
			this.logger.log('warn', 'register_validation_failed', {
				id,
				ip: client.remoteAddress,
				port: client.remotePort,
			});

			client.destroy();
		}
	}

	async unregisterClient(id) {
		if (this.instances[id]) {
			await database({
				method: 'DELETE',
				url: `/connections?connection_id=${id}&context=${this.config.module_alias}`,
			});

			this.instances[id]?.logger?.log('info', 'disconnect', {id});

			delete this.instances[id];
		}
	}

	injectBuffer(direction, target, packet) {
		const {opcode, data, encrypted, massive} = packet;

		if (typeof target === 'string') {
			switch (target) {
				case '*':
					for (const id of Object.keys(this.instances)) {
						this.instances[id].security[direction].send(opcode, data, encrypted, massive);
					}
					break;
				default:
					if (this.instances[target]) this.instances[target].security[direction].send(opcode, data, encrypted, massive);
					break;
			}
		} else {
			// specific targets on the pool:
			for (const id of target.filter((i) => Object.keys(this.instances).includes(i))) {
				this.instances[id].security[direction].send(opcode, data, encrypted, massive);
			}
		}
	}

	rateLimit(id, buffer) {
		if (this.limitter.consumeSync(id)) return this.process(id, 'client', buffer);

		// disconnect otherwise
		this.instances[id].logger.log('warn', 'packet_flood_detected', {id, buffer});
		this.disconnect([id]);
	}

	connect(id) {
		this.instances[id].connections.remote = net.connect(this.config.bindings.remote);

		// socket
		this.instances[id].connections.remote.on('data', (buffer) => this.process(id, 'remote', buffer));
		this.instances[id].connections.remote.on('error', (error) => {});
		this.instances[id].connections.remote.on('close', () => this.unregisterClient(id, 'remote'));
	}

	disconnect(instances) {
		for (const id of instances) {
			if (this.instances[id]) {
				this.instances[id].connections.remote.destroy();
				this.instances[id].connections.client.destroy();
			}
		}
	}
}

export default Network;
