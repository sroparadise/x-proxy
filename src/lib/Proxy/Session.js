import Security from '@lib/utils/security';
import NodeCache from 'node-cache';

class Session {
	constructor(id = String, ip = String, port = Number) {
		this.id = id;
		this.ip = ip;
		this.port = port;
		this.memory = new NodeCache();
		this.connections = {remote: false, client: false};
		this.security = {
			client: new Security(),
			remote: new Security(),
		};
		this.security.client.generateHandshake();
	}
}

export default Session;
