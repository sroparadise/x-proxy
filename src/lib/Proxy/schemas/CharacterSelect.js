export const header = {
	opcode: 0x7001,
	encrypted: false,
	massive: false,
};

/**
 * @module AgentServer
 * @sender client
 * @direction remote
 * @operation 0x7001
 */
export const schema = {
	CharName16: 'string',
};
