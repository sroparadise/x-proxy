export const header = {
	opcode: 0xa104,
	encrypted: false,
	massive: true,
};

/**
 * @module GatewayServer
 * @sender remote
 * @direction client
 * @operation 0xA104
 */
export const schema = {
	count: 'uint8',
	posts: {
		subject: 'string',
		article: 'string',
		year: 'uint16',
		month: 'uint16',
		day: 'uint16',
		hour: 'uint16',
		minute: 'uint16',
		second: 'uint16',
		ms: 'uint32',
	},
};
