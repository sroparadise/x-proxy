export const header = {
	opcode: 0xa10a,
	encrypted: false,
	massive: false,
};

/**
 * @module GatewayServer
 * @sender remote
 * @direction client
 * @operation 0xA10A
 * @case status: 1
 */
export const schema_success = {
	status: 'uint8',
	token: 'uint32',
	host: 'string',
	port: 'uint16',
	channel: 'uint8',
};

// // Format to be considered in future:
// export const schema_v2 = {
// 	status: 'uint8',
// 	$: {
// 		1: {
// 			token: 'uint32',
// 			host: 'string',
// 			port: 'uint16',
// 			channel: 'uint8',
// 		},
// 		2: {
// 			errorCode: 'uint8',
// 			$: {
// 				1: {
// 					maxAttempts: 'uint32',
// 					curAttempts: 'uint32',
// 				},
// 				2: {
// 					reason: 'string',
// 					year: 'uint16',
// 					month: 'uint16',
// 					day: 'uint16',
// 					hour: 'uint16',
// 					minute: 'uint16',
// 					second: 'uint16',
// 					ms: 'uint16',
// 				},
// 			},
// 		},
// 	},
// };
