export const header = {
	opcode: 0x7025,
	encrypted: false,
	massive: false,
};

/**
 * @module AgentServer
 * @sender client
 * @direction remote
 * @operation 0x7025
 */
export const schema = {
	type: 'uint8',
	index: 'uint8',
	receiver: 'string',
	message: 'text',
};
