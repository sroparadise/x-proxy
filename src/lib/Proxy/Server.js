import Network from '@lib/Proxy/Network';
import net from 'net';
import logger from '@lib/utils/logger';
import database from '@lib/utils/database';
import {FastRateLimit} from 'fast-ratelimit';

const blockList = new net.BlockList();

const limitter = new FastRateLimit({
    threshold: process.env?.FLOOD_AUTOBAN_COUNT ? parseInt(process.env.FLOOD_AUTOBAN_COUNT) || 50,
    ttl: 1,
});

class Server {
	constructor(config) {
		this.bindings = config.bindings;
		this.module = config.module;
		this.module_alias = config.module_alias;
		this.network = new Network(config);
		this.server = net.createServer();
		this.logger = logger(`${this.module}_core`, 'server');

		
	}

	validateBlockList(remote) {
		switch (true) {
			case blockList.check(remote):
				return false;
			case !limitter.consumeSync(remote):
				console.log(`[${remote}] FLOOD DETECTED - IP ADDED TO BLOCKLIST`);
				blockList.addAddress(remote);
				return false;
			default:
				return true;
		}
	}

	handleConnection(client, network) {
		const {remoteAddress, remotePort} = client;

		if (!this.validateBlockList(remoteAddress)) return client.destroy();

		const instance_id = Buffer.from(`${remoteAddress}:${remotePort}`, 'utf8').toString('base64');
		network.registerClient(instance_id, client);
	}

	async run() {
		try {
			await database({
				method: 'DELETE',
				url: `/connections?context=${this.module_alias}`,
			});

			this.server.on('connection', (instance) => this.handleConnection(instance, this.network));

			this.server.on('listening', () =>
				this.logger.log('info', 'listening', {
					module: this.module,
					bindings: this.bindings.client,
				})
			);

			this.server.on('error', (error) =>
				error?.code === 'ECONNRESET' ? () => {} : this.logger.log('error', 'client:error', error)
			);

			return this.server.listen(this.bindings.client.port, this.bindings.client.host);
		} catch (error) {
			console.log({error});
			this.logger.log('error', 'server_error', {
				message: `Failed to run - check configuration & ensure DataServer is on!`,
				data: error?.message || error?.cause || error.stack,
			});
		}
	}
}

export default Server;
