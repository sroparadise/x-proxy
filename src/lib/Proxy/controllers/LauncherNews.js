import {Reader, Writer} from '@lib/utils/security';
import {version, name, repository} from '@root/package.json';
import {schema} from '@lib/Proxy/schemas/LauncherNews';

export default async (packet, instance) => {
	// process original packet:
	const read = new Reader(packet.data);

	// read via schema:
	const data = read.fromSchema(schema);

	// instance.logger.log('info', 'launcher_news', {
	// 	data,
	// });

	const posts = [
		// some custom news:
		{
			subject: `Protected by ${name.toUpperCase()}`,
			article: `<b>Version:</b> ${version}<br>[<a href="${repository.url}" ><font color=brown>View on Gitlab</font></a>]`,
			year: 2022,
			month: 12,
			day: 12,
			hour: 12,
			minute: 59,
			second: 59,
			ms: 130000000,
		},
		...data.posts,
	];

	const writer = new Writer();

	const buffer = writer.fromSchema(schema, {
		count: posts.length,
		posts,
	});

	return [{...packet, data: buffer}];
};
