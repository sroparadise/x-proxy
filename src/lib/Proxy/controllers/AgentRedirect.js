import redirects from '@config/redirects';
import { Reader, Writer } from '@lib/utils/security';
import { schema_success } from '@lib/Proxy/schemas/AgentRedirect';

export default async (packet, instance) => {
	const read = new Reader(packet.data);

	const status = read.uint8();

	if (status === 1) {
		let data = read.fromSchema(schema_success);

		const write = new Writer();
		const redirect = redirects[`${data.host}:${data.port}`];

		if (redirect) {
			data = {
				...data,
				...redirect,
			};

			instance.logger.log('info', 'agent_redirect', {
				data,
			});
		}

		data = write.fromSchema(schema_success, data);

		return [{ ...packet, data }];
	}

	return [{ ...packet }];
};
