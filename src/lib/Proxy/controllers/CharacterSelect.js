import database from '@lib/utils/database';
import {Reader, Writer} from '@lib/utils/security';
import {schema} from '@lib/Proxy/schemas/CharacterSelect';

export default async (packet, instance, config) => {
	const read = new Reader(packet.data);

	const data = read.fromSchema(schema);

	const {
		data: {
			results: [
				{
					CharName16,
					characterLink: {
						CharID,
						user: {JID: UserJID, StrUserID, sec_primary, sec_content},
					},
				},
			],
		},
	} = await database({
		method: 'POST',
		url: '/characters/search',
		data: {
			query: {
				characterLink: {
					user: {},
				},
				$where: {
					CharName16: data.CharName16,
				},
			},
			fields: [
				'CharID',
				'characterLink.user.JID',
				'characterLink.user.StrUserID',
				'characterLink.user.StrEmail',
				'characterLink.user.sec_primary',
				'characterLink.user.sec_content',
			],
		},
	});

	const memory_init = {
		CharName16,
		CharID,
		UserJID,
		sec_primary,
		sec_content,
		StrUserID,
	};

	Object.keys(memory_init).forEach((key) => instance.memory.set(key, memory_init[key]));

	await database({
		method: 'PUT',
		url: '/connections',
		data: {
			where: {
				connection_id: instance.id,
				context: config.module_alias,
			},
			payload: {
				CharID,
				UserJID,
			},
		},
	});

	instance.logger.log('info', 'character_select', {
		data,
		session: memory_init,
	});

	return [packet];
};
