import {Reader} from '@lib/utils/security';
import {schema} from '@lib/Proxy/schemas/RequestChat';
import database from '@lib/utils/database';

const chat_types = {
	1: 'Public',
	2: 'Private',
	3: 'GM Public',
	4: 'Party',
	5: 'Guild',
	6: 'Global',
	7: 'GM Notice',
	9: 'Stall',
	11: 'Union',
	12: 'Global',
	13: 'NPC Quest',
	16: 'Academy',
};

export default async (packet, instance) => {
	const read = new Reader(packet.data);
	const data = read.fromSchema(schema);

	const UserJID = instance.memory.get('UserJID');
	const CharID = instance.memory.get('CharID');

	instance.logger.log('info', 'request_chat', {...data, chat_type: chat_types[data.index]});

	await database({
		method: 'POST',
		url: '/chat-logs',
		data: {
			UserJID,
			CharID,
			type: data.index,
			receiver: data.receiver,
			message: data.message,
		},
	});

	return [packet];
};
