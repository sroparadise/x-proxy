const {DISCORD_APP_ID, DISCORD_BOT_TOKEN} = process.env;

export default {
	id: DISCORD_APP_ID,
	token: DISCORD_BOT_TOKEN,
	commands: [
		{
			name: 'linkaccount',
			description: 'Link your game account with Discord - requires API token retrieved from the web settings section.',
		},
	],
};
