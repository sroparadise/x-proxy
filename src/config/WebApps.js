const {BIND_IP, BIND_PORT, SALT_KEY, PROXY_IP} = process.env;

export default {
	host: BIND_IP || '0.0.0.0',
	port: BIND_PORT ? parseInt(BIND_PORT) : 1330,
	salt: SALT_KEY || 'eset5ag.nsy-g6ky5.mp',
	timeout: 60,
	proxy_ip: PROXY_IP || "127.0.0.1",
};
