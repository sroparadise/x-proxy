import {AgentRedirect, LauncherNews} from '@controllers';

const {
	MODULE,
	MODULE_ALIAS,
	BIND_IP,
	BIND_PORT,
	REMOTE_IP,
	REMOTE_PORT,
	MAX_SIMULTANEOUS_IP_CONNECTIONS,
	MAX_INCOMING_BUFFER_SIZE,
	PACKET_RATE_TRESHOLD,
	PACKET_RATE_TTL,
} = process.env;

export default {
	module: MODULE || 'GatewayServer',
	module_alias: MODULE_ALIAS || MODULE || 'GatewayServer',
	// limits:
	limits: {
		MAX_SIMULTANEOUS_IP_CONNECTIONS: MAX_SIMULTANEOUS_IP_CONNECTIONS ? parseInt(MAX_SIMULTANEOUS_IP_CONNECTIONS) : 5,
		MAX_INCOMING_BUFFER_SIZE: MAX_INCOMING_BUFFER_SIZE ? parseInt(MAX_INCOMING_BUFFER_SIZE) : 192, //bytes
		PACKET_RATE_TRESHOLD: PACKET_RATE_TRESHOLD ? parseInt(PACKET_RATE_TRESHOLD) : 65,
		PACKET_RATE_TTL: PACKET_RATE_TTL ? parseInt(PACKET_RATE_TTL) : 5,
	},
	// proxy server ip:port and remote server ip:port
	bindings: {
		// Proxy Server IP:PORT [*client] ===> [proxy]
		client: {
			host: BIND_IP || '0.0.0.0',
			port: BIND_PORT ? parseInt(BIND_PORT) : 15779,
		},

		// Gateway Server IP:PORT [*proxy] -> [gatewayserver)]
		remote: {
			host: REMOTE_IP || '127.0.0.1',
			port: REMOTE_PORT ? parseInt(REMOTE_PORT) : 8526,
		},
	},
	controllers: {
		// Packet direction: Client -> Server
		client: {
			// OPCODE: CONTROLLER_NAME
		},

		// Packet direction: Server -> Client
		remote: {
			0xa10a: AgentRedirect,
			0xa104: LauncherNews,
		},
	},
	whitelist: [0x6104, 0x6100, 0x6107, 0x6101, 0x610a, 0x6117],
};
