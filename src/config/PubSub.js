export default {
    module: {
        host: "127.0.0.1",
        port: 6430,
    },
    web: {
        host: "127.0.0.1",
        port: 6431,
    }
};