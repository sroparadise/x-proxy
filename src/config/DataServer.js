const {BIND_API_HOST, BIND_API_PORT, NODE_ENV} = process.env;

export default {
	host: BIND_API_HOST || "0.0.0.0",
	port: BIND_API_PORT ? parseInt(BIND_API_PORT) : 6440,
	development: NODE_ENV && NODE_ENV === 'development',
};
