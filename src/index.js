// import cluster from 'cluster';
import dotenv from 'dotenv';
import * as services from '@services';
import {cpus} from 'os';
import logger_instance from '@lib/utils/logger';
import path from 'path';

dotenv.config({
	path: path.join(process.cwd(), '.env'),
});

const module = process.env.MODULE;
const log_name = `${module}_runner`;
const logger = logger_instance(log_name, 'local');

logger.log('info', 'module:init', {
	module,
	pid: process.pid,
});

new services[module]().run();
console.info(`[${module}] running.`);

process.on('unhandledRejection', (error, promise) => {
	console.log({unhandledRejection: error});
	promise.catch();
});

process.on('uncaughtException', async (error) => {
    console.log({uncaughtException: error});
});
