USE SILKROAD_R_ACCOUNT
GO
CREATE OR ALTER TRIGGER [dbo].[CreateDiscordToken] ON [dbo].[TB_User]
AFTER INSERT AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @JID BIGINT;
	DECLARE @TOKEN VARCHAR(32);
	
	SET @JID = (SELECT JID FROM inserted);
	SET @TOKEN = (SELECT REPLACE(NEWID(), '-', '') AS Random32);
	
	INSERT INTO SILKROAD_PROXY.dbo.api_tokens (UserJID, token) VALUES (@jid, @TOKEN);
END