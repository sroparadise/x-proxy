@echo off
title DataServer
cd ..
set NODE_ENV=production
set MODULE=DataServer
set WORKER_COUNT=1

yarn DataServer
pause