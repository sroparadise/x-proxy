@echo off
title AgentServer:1
cd ..
set NODE_ENV=development
set MODULE=AgentServer
set MODULE_ALIAS=AgentServer_1
set IPC=1
set WORKER_COUNT=1

yarn AgentServer:1
pause