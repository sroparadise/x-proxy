@echo off
title AppServer
cd ..
set NODE_ENV=production
set MODULE=WebApps
set WORKER_COUNT=1

yarn WebApps
pause