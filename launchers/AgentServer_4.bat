@echo off
title AgentServer:4
cd ..
set NODE_ENV=development
set MODULE=AgentServer
set MODULE_ALIAS=AgentServer_4
set IPC=1
set WORKER_COUNT=1

yarn AgentServer:4
pause