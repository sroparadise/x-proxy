@echo off
title GatewayServer
cd ..
set NODE_ENV=development
set MODULE=GatewayServer
set WORKER_COUNT=1
yarn GatewayServer
pause