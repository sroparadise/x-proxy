@echo off
title AgentServer:2
cd ..
set NODE_ENV=development
set MODULE=AgentServer
set MODULE_ALIAS=AgentServer_2
set IPC=1
set WORKER_COUNT=1

yarn AgentServer:2
pause