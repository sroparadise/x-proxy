@echo off
title AgentServer:3
cd ..
set NODE_ENV=development
set MODULE=AgentServer
set MODULE_ALIAS=AgentServer_3
set IPC=1
set WORKER_COUNT=1

yarn AgentServer:3
pause