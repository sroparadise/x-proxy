import {useEffect} from 'react';
import {set} from 'config/store/reducers/appState';
import Box from '@mui/material/Box';
import {styled} from '@mui/material/styles';
import Paper from '@mui/material/Paper';
import Masonry from '@mui/lab/Masonry';
import FadeIn from 'lib/components/FadeIn';
import {useTheme} from '@mui/material/styles';
import useMediaQuery from '@mui/material/useMediaQuery';
import {useDispatch} from 'react-redux';
import { Budget } from 'lib/components/dashboard/budget';
import { TotalCustomers } from 'lib/components/dashboard/total-customers';
import { TotalProfit } from 'lib/components/dashboard/total-profit';

const components = [
  <Budget />,
  <TotalProfit />,
  <TotalCustomers />,
];

const Item = styled(Paper)(({theme}) => ({
	backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
	...theme.typography.body2,
	padding: theme.spacing(0.5),
	textAlign: 'center',
	color: theme.palette.text.secondary,
}));

function Dashboard() {
	const theme = useTheme();
	
  const is600 = useMediaQuery(theme.breakpoints.up('sm'));
	const is900 = useMediaQuery(theme.breakpoints.up('md'));
	const is1200 = useMediaQuery(theme.breakpoints.up('lg'));
	const is1536 = useMediaQuery(theme.breakpoints.up('sl'));

	const dispatch = useDispatch();

	const getColumns = () => {
		switch (true) {
			case is600 && !is900:
				return 1;
			case is900 && !is1200:
				return 3;
			case is1200 && !is1536:
				return 3;
			case is1536:
				return 4;
			default:
				return 1;
		}
	};

	useEffect(() => {
		dispatch(
			set({
				breadcrumbs: [
					{
						name: 'Dashboard',
						url: '/',
					},
				],
			})
		);
	}, []);

	return (
		<Box>
			<Masonry columns={getColumns()} spacing={2}>
				{components.map((component, index) => (
					<FadeIn key={index} delay={(index + 1) * 50}>
						<Item className="aos-animate">
							{component}
						</Item>
					</FadeIn>
				))}
			</Masonry>
		</Box>
	);
}

export default Dashboard;
