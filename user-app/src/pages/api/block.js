import axios from 'axios';

const {API_HOST} = process.env;

export default async function handler(req, res) {
	if (req.method === 'POST') {
		const {
			body: {connections},
		} = req;

		const keys = Object.keys(connections);
		let blocklist = [];

		for await (const key of keys) {
			await axios.post(`${API_HOST}/publish`, {
				context: key,
				operation: 'disconnect',
				data: connections[key],
			});

			blocklist = [...blocklist, ...[connections[key]]];
		}

		// use blocklist map with payloads & insert into blocklist db
		// const payload_block = blocklist.map(i => )

		// console.log(payload_block);

		res.status(200).end();
	} else {
		res.status(404).end();
	}
}
