import {useSelector} from 'react-redux';
import Authentication from 'lib/components/Authentication';
import Registration from 'lib/components/Registration';
import { useState } from 'react';

const withAuthentication = (Component) => {
	return () => {
		const {isAuthenticated} = useSelector((state) => state.appState);
		
		const [state, setState] = useState({
			section: 'login'
		});

		return isAuthenticated ? (
			<Component />
		) : (
			<>
				<Authentication />
			</>
		);
	};
};

export default withAuthentication;
